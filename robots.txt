User-agent: *
Disallow: /application
Disallow: /application/views/legal/law-1819-of-2016.php
Disallow: /assets
Disallow: /bk
Disallow: /media
Disallow: /system
Disallow: /others
Disallow: /vendor
Allow: /www.rootdevel.org/application/views

User-agent: Baiduspider
Disallow: /

User-agent: baiduspider
Disallow: /