<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sliders
 *
 * This library build all of sliders of the website
 * you can be call functions in your views.
 *
 */

 class Sliders {

 function __construct() {
 }

 public function buildLandingSlider(){
   $landing_slider = 'media/landingSlider';
   $dirint = dir($landing_slider);
   while (($image = $dirint->read()) !== false)
    {
      if (preg_match('/'.'gif'.'/', $image) || preg_match('/'.'jpg'.'/', $image) || preg_match('/'.'png'.'/', $image))
      {
        echo '<article>';
        echo '<img src="'.$landing_slider.'/'.$image.'" alt="" data-position="bottom right" />';
        echo '</article>';
      }
    }
    $dirint->close();

 }
}
