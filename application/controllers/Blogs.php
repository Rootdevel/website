<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'blogs';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

    public function micropython()
	{
		$data['wrapper'] = 'blogs/micropython';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

    public function dataprevention()
	{
		$data['wrapper'] = 'blogs/discurso-privacidad';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}


  public function hackspacemagazine()
	{
		$data['wrapper'] = 'blogs/hackspace-magazine';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
	
  public function hackerspaces()
	{
		$data['wrapper'] = 'blogs/hackerspaces';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}


}

