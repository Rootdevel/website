<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'team/freelancer';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
}
