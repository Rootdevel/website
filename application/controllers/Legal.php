<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'legal';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function glosary()
	{
		$data['wrapper'] = 'legal/glosary';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function convivence()
	{
		$data['wrapper'] = 'legal/convivence';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function terms()
	{
		$data['wrapper'] = 'legal/terms';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function law1819()
	{
		$data['wrapper'] = 'legal/law-1819-of-2016';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
}
