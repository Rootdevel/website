<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'landing';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
}
