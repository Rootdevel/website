<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'events';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function cncnumeric()
	{
		$data['wrapper'] = 'events/cncnumeric';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function flisol2014()
	{
		$data['wrapper'] = 'events/flisol2014';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function sfd2012()
	{
		$data['wrapper'] = 'events/sfd-2012';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function thinkbig2017()
	{
		$data['wrapper'] = 'events/thinkbig-2017';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
}
