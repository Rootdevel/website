<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'about/institutional';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}
}
