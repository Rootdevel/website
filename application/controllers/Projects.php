<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {


	public function index()
	{
		$data['wrapper'] = 'projects';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

    public function cncnumeric()
	{
		$data['wrapper'] = 'projects/cncnumeric';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}


	public function thinkbig2017()
	{
		$data['wrapper'] = 'projects/thinkbig-2017';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

	public function prusa3d ()
	{
		$data['wrapper'] = 'projects/3d-printer-prusai3';

		$this->load->view('head',$data);
		$this->load->view('wrapper',$data);
	}

}
