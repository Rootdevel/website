<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Footer -->
										<footer id="footer">
											<section class="links">
												<div>
													<h3>Instituciónal</h3>
													<ul class="plain">
														<li><a href="<?= base_url() ?>about">Acerca de Rootdevel</a></li>
														<li><a href="<?= base_url() ?>">Planes y Procesos</a></li>
														<li><a href="https://gitlab.com/Rootdevel/Institutional-Identity">Uso de Marca</a></li>
													</ul>
												</div>
												<div>
													<h3>Entradas</h3>
													<ul class="plain">
													    <li><a href="<?= base_url() ?>blogs">Blogs</a></li>
														<li><a href="<?= base_url() ?>projects">Proyectos</a></li>
														<li><a href="<?= base_url() ?>events">Eventos</a></li>
														<!--<li><a href="#">Noticias</a></li>-->
													</ul>
												</div>
												<div>
													<h3>Más Información</h3>
													<ul class="plain">
														<li><a href="<?= base_url() ?>legal/glosary">Glosario</a></li>
														<li><a href="<?= base_url() ?>">Donaciones</a></li>
														<li><a href="<?= base_url() ?>legal/convivence">Normas de Convivencia</a></li>
													</ul>
												</div>
												<div>
													<h3>Legal</h3>
													 <ul class="plain">
													     <li><a href="<?= base_url() ?>legal/law1819">Ley 1819 del 2016</a></li>
														 <li><a href="<?= base_url() ?>legal/terms">Términos y Condiciones</a></li>
													 </ul>
												</div>
											</section>
											<ul class="contact-icons">
												<li class="icon fa-home"><a href="#">Sogamoso, Boyacá, Colombia</a></li>
												<li class="icon fa-phone"><a href="#">(+57) 319 315 4264</a></li>
												<li class="icon fa-envelope-o"><a href="mailto:info@rootdevel.org">info@rootdevel.org</a></li>
											</ul>
											<p class="copyright">Licenciado bajo <a href="https://creativecommons.org/licenses/by-sa/2.5/co/">Creative Commons Atribución-Compartir Igual 2.5 Colombia (CC BY-SA 2.5 CO)</a></p>
											<ul class="icons">
												<li><a target="_blank" href="http://www.facebook.com/rootdevel" class="icon fa-facebook fa-lg"><span class="label">Facebook</span></a></li>
												<li><a target="_blank" href="http://twitter.com/rootdevel" class="icon fa-twitter fa-lg"><span class="label">Twitter</span></a></li>
												<li><a target="_blank" href="http://t.me/rootdevel" class="icon fa-telegram fa-lg"><span class="label">Telegram</span></a></li>
												<li><a target="_blank" href="https://www.flickr.com/photos/rootdevel_hackerspace/" class="icon fa-flickr fa-lg"><span class="label">Instagram</span></a></li>
												<li><a target="_blank" href="https://gitlab.com/Rootdevel" class="icon fa-gitlab fa-lg"><span class="label">GitHub</span></a></li>
												<li><a target="_blank" href="http://www.meetup.com/rootdevel" class="icon fa-meetup fa-lg"><span class="label">Meet Up</span></a></li>
													<li><a target="_blank" href="https://wiki.hackerspaces.org/Rootdevel_Hackerspace" class="icon fa fa-rocket fa-lg"><span class="label">Hackerspaces</span></a></li>
												<!--<li><a target="_blank" href="https://www.kickstarter.com/profile/rootdevel" class="icon fa-meetup fa-lg"><span class="label">kickstarted</span></a></li>-->
											</ul>
										</footer>
