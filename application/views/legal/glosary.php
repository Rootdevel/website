<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>GLOSARIO</h1>
					</header>

          <hr />

					<!--<a href="#" class="image main"><img src="<?= base_url('application/media/images/') ?>" alt="" /></a>_-->

					<ul>
						<li><b>Hacker:</b> Persona con grandes grandes habilidades técnicas en computación​​​​​​​ que sobrepasan  cualquier usuario final; Los hackers resuelven problemas y construyen cosas. Ellos creen en la libertad y la ayuda voluntaria mutua.</li><br />
						<li><b>Cracker:</b> personas que rompen o vulneran algún sistema de seguridad​​​​​​​ ilegalmente.</li><br />
						<li><b>Hackerspace:</b> Es un sitio físico donde gente con intereses en ciencia, nuevas tecnologías, y artes digitales o electrónicas se puede conocer, socializar y colaborar. Puede ser visto como un laboratorio de comunidad abierta, un espacio donde gente de diversos trasfondos puede unirse. Pone al alcance de aficionados y estudiantes de diferentes niveles la infraestructura y ambiente necesarios para desarrollar sus proyectos tecnológicos. El propósito de un hackspace es concentrar recursos y conocimiento para fomentar la investigación y el desarrollo.</li><br />
						<li><b>Do It Yourself (D.I.Y):</b> Hazlo tú mismo, es la práctica de la fabricación o reparación de cosas por uno mismo, de modo que ahorra dinero, se entretiene y se aprende al mismo tiempo.</li><br />
						<li><b>Aprendizaje colaborativo:</b> Es una técnica didáctica que promueve el aprendizaje centrado en el alumno basando el trabajo en pequeños grupos, donde los estudiantes con diferentes niveles de habilidad utilizan una variedad de actividades de aprendizaje para mejorar su entendimiento sobre una materia. Cada miembro del grupo de trabajo es responsable no solo de su prendizaje, sino de ayudar a sus compañeros a aprender, creando con ello una atmósfera de logro.  Los estudiantes trabajan en una tarea hasta que los miembros del grupo la han completado exitosamente.</li><br />
						<li><b>Economía colaborativa:</b> modelo fundado en la colaboración y la ayuda mutua como una forma diferente de economía, que depende mucho más del capital social que del capital de mercado, el cual se basa en prestar, alquilar, trocar, comprar o vender productos o servicios en función de necesidades específicas.</li><br />
						<li><b>Desarrollo sostenible:</b> aquél desarrollo que es capaz de satisfacer las necesidades actuales sin comprometer los recursos y posibilidades de las futuras generacione​​​​​​​s.</li><br />
						<li><b>Proyectos colaborativos:</b>  Los proyectos colaborativos convocan a los participantes a sumar esfuerzos, competencias y habilidades, mediante una serie de trabajos en colaboración e interacciones que les permiten alcanzar juntos un propósito común.</li><br />​​​​​​​
						<li><b>Business Process Model and Notation (BPMN):</b>Es una notación gráfica estandarizada que modela, gestiona y optimiza los procesos de la organización y por ende la política de sus aplicativos, como un lenguaje común para cerrar la brecha de comunicación que frecuentemente se presenta entre el diseño de los procesos y su implementación.​​​​​​​</li><br />
						<li><b>Fraternidad:</b>  sinónimo de hermandad como hermanos y, por extensión, de amistad o de camaradería, puede referirse: A una organización de personas que comparten una amistad y gustos particulares​​​​.​</li><br />
						<li><b>Interdisciplinar:</b>  se refiere a la habilidad para combinar varias disciplinas, es decir para interconectarlas y ampliar de este modo las ventajas que cada una ofrece. Se refiere no sólo a la aplicación de la teoría en la práctica, sino también a la integración de varios campos en un mismo trabajo​​​​​​​.</li><br />
						<li><b>Conocimiento libre:</b>  corriente epistemológica que estudia el origen histórico y el valor del conocimiento considerándolo como un bien público que beneficia a la colectividad en general y permite el desarrollo igualitario.​​​​​</li><br />​
						<li><b>Cultura libre:</b>  es una corriente de pensamiento que promueve la libertad en la distribución y modificación de trabajos creativos basándose en el principio del contenido libre para distribuir o modificar trabajos y obras creativas​​​​​​​​​​​​​​.</li><br />
						<li><b>Software libre:</b>  conjunto de software (programa informático) que por elección manifiesta de su autor, puede ser copiado, estudiado, modificado, utilizado libremente con cualquier fin y redistribuido con o sin cambios o mejoras​​​​​​.</li><br />
						<li><b>Hardware libre:</b>  dispositivos de hardware cuyas especificaciones y diagramas esquemáticos son de acceso público,​​​​​​​ cuyas especificaciones están disponibles bajo una licencia libre.​​​​​​</li><br />
						<li><b>Hackthones:</b>  viene de la suma de las palabras​​​​​​​ hack+maratón​​​​​​​ encuentro de programadores y desarrolladores con el objetivo de encontrar una solución a un problema de forma colaborativa durante un plazo determinado.</li><br />​​​​​​​
						<li><b>Barcamps:</b>   desconferencias (eventos abiertos y participativos)​​​​​​​ cuyo contenido es provisto por los participantes​​​​​​​. su temática esta  alrededor de temas sociales, artísticos, educativos... con fuertes componentes creativos e innovadores en los respectivos ámbitos.​​​​​</li><br />​
						<li><b>Praxis:</b>  Ejercicio practico de la teoria.</li><br />​
				</section>

		</div>
	</div>
