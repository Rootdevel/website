<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>TÉRMINOS Y POLITICAS DE PRIVACIDAD</h1>
					</header>

          <hr />

					<!--<a href="#" class="image main"><img src="<?= base_url('/media/images/') ?>application/media/images/sfd2012.jpg" alt="" /></a>_-->

					<h3>POLÍTICAS DE TRATAMIENTO Y LINEAMIENTOS DE PROTECCIÓN DE DATOS PERSONALES</h3>
					<p align="justify">En la <b>Fundación Rootdevel Hackerspace</b> somos conscientes que la privacidad, la intimidad y el hábeas data son derechos fundamentales. Por tal motivo, como responsables del tratamiento de datos personales  (Ley 1266 de 2008, Ley 1581 de 2012, Decreto 1074 de 2015 y demás normas que los modifiquen, complementen o sustituyan), queremos que sepas cuáles son los datos personales que recolectamos en el desarrollo de nuestro objeto social y cómo los usamos.</p>

					<h3>A. Responsables</h3>
					<p align="justify"><b><b>Fundación Rootdevel Hackerspace</b></b> Nit. 900684703-9  Representante Legal: OSCAR DAVID REYES HERNANDEZ Dirección: Carrera 9ª a No 15-54 Sogamoso, Boyacá Código Postal: 152210 Teléfono: 319 3154264 Correo Electrónico: info@rootdevel.org POLÍTICA DE TRATAMIENTO DE LOS DATOS PERSONALES: Los datos proporcionados a la <b>Fundación Rootdevel Hackerspace</b> serán objeto de tratamiento (recolección, almacenamiento, uso, circulación, o supresión) para la finalidad específica para la que fueron suministrados y el cumplimiento de las funciones constitucionales y de los requisitos previstos en la Ley 1581 de 2012 para el tratamiento de datos personales.</p>

					<h3>B. Aviso de privacidad</h3>
					<p align="justify">“En cumplimiento de la Ley 1581 de 2012 “Por la cual se dictan disposiciones generales para la protección de datos personales” y el Decreto 1377 de 2013, La <b>Fundación Rootdevel Hackerspace</b> identificada con Nit. 900684703-9, en su calidad de responsable del tratamiento de datos personales, informa su política de tratamiento y los lineamientos de protección de datos personales a través de su página web a partir del miércoles, 24 de Enero de 2018.  Los datos personales proporcionados a la <b>Fundación Rootdevel Hackerspace</b> serán objeto de tratamiento para la finalidad para la cual ha sido recolectada, esto es, para mantener un contacto efectivo y enriquecedor con usted, al contarle de nuestros programas, su accionar y logros alcanzados, nuestras proyecciones como Fundación y los principales objetivos obtenidos de nuestra gestión, así como para fines sociales, contractuales, estadísticos.</p>

					<h3>C. Definiciones</h3>
					<ol>
						<li><b>Titular:</b> Persona natural cuyos datos personales sean objeto de Tratamiento.</li>
					   	<li><b>Responsable del tratamiento:</b> Es la Fundación o cualquier otra persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, decida sobre el Tratamiento de los datos.</li>
					    <li><b>Autorización:</b> Consentimiento previo, expreso e informado del Titular para llevar a cabo el tratamiento de datos personales.</li>
					    <li><b>Datos personales:</b> Cualquier información vinculada o que pueda asociarse a una o varias personas naturales determinadas o determinables.</li>
					    <li><b>Tratamiento:</b> Cualquier operación o conjunto de operaciones sobre datos personales, tales como la recolección, almacenamiento, uso, circulación o supresión.</li>
						<li><b>Página web:</b> Es la página virtual de la Fundación la cual se encuentra en la dirección virtual: <a href="<?= base_url() ?>">https://www.rootdevel.org</a></li>
						<li><b>Aviso de privacidad:</b> Documento físico, electrónico o en cualquier otro formato generado por el Responsable que se pone a disposición del Titular para el tratamiento de sus datos personales. En el Aviso de Privacidad se comunica al Titular la información relativa a la existencia de las políticas de tratamiento de información que le serán aplicables, la forma de acceder a las mismas y la finalidad del tratamiento que se pretende dar a los datos personales.</li>
				     	 <li><b>Base de Datos:</b> Conjunto organizado de datos personales que sea objeto de Tratamiento.</li>
				        <li><b>Dato público:</b> Es el dato calificado como tal según los mandatos de la ley o de la Constitución Política y aquel que no sea semiprivado, privado o sensible. Son públicos, entre otros, los datos relativos al estado civil de las personas, a su profesión u oficio, a su calidad de comerciante o de servidor público y aquellos que puedan obtenerse sin reserva alguna. Por su naturaleza, los datos públicos pueden estar contenidos, entre otros, en registros públicos, documentos públicos, gacetas y boletines oficiales.</li>
						<li><b>Dato privado:</b> Es el dato que por su naturaleza íntima o reservada sólo es relevante para el titular.</li>
						<li><b>Datos sensibles:</b> Se entiende por datos sensibles aquellos que afectan la intimidad del Titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición, así como los datos relativos a la salud, a la vida sexual y los datos biométricos.</li>
						</ol>

					<h3>D. Principios aplicables al tratamiento de datos personales</h3>
					<p>El tratamiento de datos personales en la dirección  web  https://www.rootdevel.org, se regirá por los siguientes principios:</p>
					<ol>
						<li><b>Principio de finalidad:</b> El Tratamiento de los datos personales recogidos debe obedecer a una finalidad legítima, la cual debe ser informada al Titular.</li>
						<li><b>Principio de libertad:</b> El Tratamiento sólo puede llevarse a cabo con el consentimiento, previo, expreso e informado del Titular. Los datos personales no podrán ser obtenidos o divulgados sin previa autorización, o en ausencia de mandato legal o judicial que releve el consentimiento;</li>
						<li><b>Principio de veracidad o calidad:</b> La información sujeta a Tratamiento debe ser veraz, completa, exacta, actualizada, comprobable y comprensible. No será́ efectuado el Tratamiento de datos parciales, incompletos, fraccionados o que induzcan a error;</li>
						<li><b>Principio de transparencia:</b> En el Tratamiento debe garantizarse el derecho del Titular a obtener de la <b>Fundación Rootdevel Hackerspace</b>, en cualquier momento y sin restricciones, información acerca de la existencia de datos que le conciernan;</li>
						<li><b>Principio de acceso y circulación restringida:</b> Los datos personales, salvo la información pública, y lo dispuesto en la autorización otorgada por el titular del dato, no podrán estar disponibles en Internet u otros medios de divulgación o comunicación masiva, salvo que el acceso sea técnicamente controlable para brindar un conocimiento restringido sólo a los Titulares o terceros autorizados;</li>
						<li><b>Principio de seguridad:</b> La información sujeta a Tratamiento por parte de la <b>Fundación Rootdevel Hackerspace</b>, se deberá́ proteger mediante el uso de las medidas técnicas, humanas y administrativas que sean necesarias para otorgar seguridad a los registros evitando su adulteración, perdida, consulta, uso o acceso no autorizado o fraudulento;</li>
						<li><b>Principio de confidencialidad:</b> Todas las personas que intervengan en el Tratamiento de datos personales están obligadas a garantizar la reserva de la información, inclusive después de finalizada su relación con alguna de las labores que comprende el Tratamiento.</li>
					</ol>
					<p><b>PARÁGRAFO PRIMERO:</b> En el evento que se recolecten datos personales sensibles, el Titular podrá negarse a autorizar su Tratamiento.</p>

					<h3>E. Datos personales de niños, niñas y adolescentes</h3>
					<p align="justify">Es posible que la <b>Fundación Rootdevel Hackerspace</b> reciba o haya recibido datos de niños, niñas y adolescentes que acceden a sus servicios, especialmente los de naturaleza cultural, por lo que el suministro de sus datos personales es de carácter facultativo.</p>
					<p align="justify">La <b><b>Fundación Rootdevel Hackerspace</b></b> velará por el uso adecuado de los datos personales de los niños, niñas y adolescentes y respetará en su tratamiento el interés superior de aquellos, asegurando la protección de sus derechos fundamentales y, en lo posible, teniendo en cuenta su opinión como titulares de sus datos personales.  DERECHOS DE LOS TITULARES DE LOS DATOS PERSONALES: Los titulares de los datos personales podrán ejercer sus derechos para acceder, conocer, actualizar y rectificar dichos datos; ser informado sobre el uso dado a los mismos y la autorización con que se cuenta para ello; presentar consultas y reclamos; revocar la autorización o solicitar la supresión de sus datos, en los casos en que sea procedente pueden hacerlo a través del correo electrónico: info@rootdevel.org  o en el Teléfono: 319 3154264.</p>

					<h3>F. Ejercicio de los derechos de los titulares de los datos personales</h3>
					<p align="justify">​​​​​​​De acuerdo con las disposiciones contenidas en las normas citadas le informamos que los derechos que le asisten como titular de la información son:</p>
					<ol>
					<li>Conocer, actualizar y rectificar sus datos personales frente a los Responsables del Tratamiento o Encargados del Tratamiento. Este derecho se podrá ejercer, entre otros frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo Tratamiento este expresamente prohibido o no haya sido autorizado;</li>
					<li>Solicitar prueba de la autorización otorgada al Responsable del Tratamiento salvo cuando expresamente se exceptúe como requisito para el Tratamiento, de conformidad con lo previsto en el artículo 10 de la ley 1581 de 2012;</li>
					<li>Ser informado por el Responsable del Tratamiento o el Encargado del Tratamiento, previa solicitud, respecto del uso que le ha dado a sus datos personales;</li>
					<li>Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la presente ley y las demás normas que la modifiquen, adicionen o complementeno</li>
					<li>Revocar la autorización y/o solicitar la supresión del dato cuando en el Tratamiento no se respeten los principios, derechos y garantías constitucionales y legales.</li>
				    <li>Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento.</li>
					<li>El Titular podrá consultar de forma gratuita sus datos personales:</li>
					<ol>
					<li>Al menos una vez cada mes calendario,
					<li>y cada vez que existan modificaciones sustanciales de las Políticas de Tratamiento de la información que motiven nuevas consultas.</li>
					</ol>
					<p><b>Nota:</b> La revocatoria y/o supresión procederá cuando la Superintendencia de Industria y Comercio haya determinado que en el Tratamiento el Responsable o Encargado han incurrido en conductas contrarias a esta ley y a la Constitución.</p>
					<p align="justify">Para consultas cuya periodicidad sea mayor a una por cada mes calendario, el responsable solo podrá cobrar al titular los gastos de envío, reproducción y, en su caso, certificación de documentos. Los costos de reproducción no podrán ser mayores a los costos de recuperación del material correspondiente. Para ejercer esos derechos, puede contactarse al correo electrónico: info@rootdevel.org  o en el Teléfono: 319 3154264.</p>
					</ol>

					<h3>F.1. Consultas</h3>
					<p align="justify">Los titulares de los datos personales o sus causahabientes podrán consultar la información personal del titular que repose en cualquier base de datos de la <b>Fundación Rootdevel Hackerspace</b>, la cual suministrará a estos toda la información contenida en el registro individual o que esté vinculada con la identificación del titular.</p>
					<p align="justify">La consulta podrá ser formulada a través de los diferentes medios físicos o electrónicos habilitados para ello, siempre y cuando se pueda mantener prueba de ésta.</p>
					<p align="justify">La consulta será atendida de conformidad con el término establecido en la ley, contados a partir de la fecha de recibo de la misma en las instalaciones de la <b>Fundación Rootdevel Hackerspace</b>, cuando se trate de un medio físico o en el correo electrónico info@rootdevel.org cuando sea través de un medio electrónico. Cuando no fuere posible atender la consulta dentro de dicho término, se informará al interesado, expresando los motivos de la demora y señalando la fecha en que se atenderá su consulta, la cual en ningún caso podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término.</p>

					<h3>F.2. Reclamos</h3>
					<p align="justify">El titular de los datos personales o sus causahabientes que consideren que la información contenida en una base de datos debe ser objeto de corrección, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en la ley, podrán presentar un reclamo ante la <b>Fundación Rootdevel Hackerspace</b>, el cual será tramitado bajo las siguientes reglas:</p>

					<ol>
						<li>El reclamo se formulará mediante correo electrónico a (info@rootdevel.org), incluyendo la descripción de los hechos que dan lugar al reclamo, la dirección, y acompañando los documentos que se quiera hacer valer. Si el reclamo resulta incompleto, se requerirá al interesado dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido del reclamo.</li>
						<li>En caso de que quien reciba el reclamo no sea competente para resolverlo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al interesado.</li>
						<li>Una vez recibido el reclamo completo, se incluirá en la base de datos una leyenda que diga “reclamo en trámite” y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Dicha leyenda deberá mantenerse hasta que el reclamo sea decidido.</li>
						<li>El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.</li>
					</ol>
					<p align="justify"><b>Nota:</b> en el asunto de la solicitud se deberá indicar que se trata de datos personales y especificar si se trata de datos de cliente, colaborador, pensionado, estudiante, aspirante, contratista o proveedor, o usuario en general.</p>

				<h3>G. Finalidad con la que se efectúa la recolección de datos personales y tratamiento de los mismos</h3>
				<p align="justify">Además de las finalidades generales, existen finalidades particulares, atendiendo a la relación que tienen las personas con la <b>Fundación Rootdevel Hackerspace</b>, como a continuación se describen:</p>

				<h3>G.1. Finalidades especiales para el tratamiento de los datos de clientes, sus colaboradores y sus funcionarios:</h3>
				<ol>
					<li>Lograr una eficiente comunicación relacionada con nuestros servicios y alianzas, por diferentes medios.</li>
					<li>Ofrecer información sobre campañas, programas especiales.</li>
					<li>Informar e invitar a campañas de mercadeo, promoción de servicios y educación al usuario.</li>
					<li>Realizar encuesta de satisfacción de servicios y atenciones prestadas.</li>
					<li>Contestación, gestión y seguimiento a solicitudes de mejoramiento, peticiones y sugerencias.</li>
					<li>Evolución de la calidad del servicio.</li>
					<li>Enviar al correo físico, electrónico, celular o dispositivo móvil, vía mensajes de texto (SMS y/o MMS) o a través de cualquier otro medio análogo y/o digital de comunicación creado o por crearse, información sobre los servicios que presta, los eventos que programe, con el fin de impulsar, invitar, dirigir, ejecutar e informar.</li>

				</ol>

				<h3>G.2. Finalidades especiales para el tratamiento de los datos personales de colaboradores y funcionarios de la Fundación Rootdevel Hackerspace</h3>
				<ol>
				<li>Realización de publicaciones internas y externas.</li>
				<li>Apertura de acceso a plataformas tecnológicas propias de la organización.</li>
				<li>Brindar información a empresas que solicitan verificar datos laborales de los empleados para autorización de créditos de dinero o créditos comerciales. (Previa verificación de fuente y uso de datos, se debe centrar en la verificación más no en el suministro de la información).</li>
				<li>Ser contactado directamente en caso de ser requerido, en razón de sus funciones.</li>
				<li>Detectar las necesidades de capacitación e implementar acciones que permitan una excelente prestación de los servicios brindados por la <b>Fundación Rootdevel Hackerspace</b>.</li>
				<li>Informar y conformar procesos de elección y promoción interna.</li>
				<li>Soportar procesos de auditoría interna o externa.</li>
				</ol>

				<h3>G.3. Finalidades especiales de los datos personales de visitantes y otros usuarios de la comunidados</h3>

				<ol>
					<li>Seguridad de visitantes, colaboradores y de la comunidad general que durante su permanencia en las instalaciones de la <b>Fundación Rootdevel Hackerspace</b>.</li>
				</ol>
					<p>Respecto de los datos:</p>
				<ol>
					<li>Recolectados directamente en las instalaciones o sitio web de la <b>Fundación Rootdevel Hackerspace</b></li>
					<li>Tomados de los documentos que suministran las personas a los funcionarios o colaboradores,</li>
					<li>Obtenidos de las videograbaciones que se realizan dentro o fuera de las instalaciones de la <b>Fundación Rootdevel Hackerspace</b> y/o</li>
					<li>Adquiridos por medio de cualquier mecanismo legalmente establecido,</li>
				</ol>
					<p align="justify">Éstos se utilizarán para el registro en las bitácora de gestión, para fines de seguridad de las personas, los bienes e instalaciones de la <b>Fundación Rootdevel Hackerspace</b> y podrán ser utilizados como prueba en cualquier tipo de proceso.</p>
					<p>Si un dato personal es proporcionado, dicha información será utilizada sólo para los propósitos aquí señalados, y por tanto, la <b>Fundación Rootdevel Hackerspace</b>, no procederá a vender, licenciar, transmitir, o divulgar la misma, salvo que:</p>
				<ol>
					<li>Exista autorización expresa para hacerlo.</li>
					<li>Que sea requerido o permitido por la ley.</li>
				</ol>
					<p align="justify">la <b>Fundación Rootdevel Hackerspace</b>, podrá subcontratar a terceros para el procesamiento de determinadas funciones o información. Cuando efectivamente se subcontrate con terceros el procesamiento de información personal o se proporcione información personal a terceros prestadores de servicios, la <b>Fundación Rootdevel Hackerspace</b>, advierte a dichos terceros sobre la necesidad de proteger dicha información personal con medidas de seguridad apropiadas, se prohíbe el uso de la información para fines propios y se solicita que no se divulgue la información personal a otros.</p>

					<h3>H. Tratamiento al cual podrán ser sometidos los datos personales</h3>
					<ol>
					<li>Consulta.</li>
						<li>Trámites administrativos.</li>
						<li>Envío de información.</li>
						<li>Investigación.</li>
						<li>Solicitud de diligenciamiento de encuestas.</li>
						<li>Realización de llamadas.</li>
					</ol>
					<p><b>Nota:</b> frente al tratamiento de los datos sensibles, la ley permite que estos sean tratados únicamente cuando:</p>
					<ol>
						<li>El titular de los datos personales haya dado su autorización explícita a dicho tratamiento, salvo en los casos que por ley no sea requerido el otorgamiento de dicha autorización.</li>
						<li>El tratamiento sea necesario para salvaguardar el interés vital del titular de los datos personales y este se encuentre física o jurídicamente incapacitado. En estos eventos, los representantes legales deberán otorgar su autorización.</li>
						<li>El tratamiento sea efectuado en el curso de las actividades legítimas y con las debidas garantías por parte de una fundación, ONG, asociación o cualquier otro organismo sin ánimo de lucro, cuya finalidad sea política, filosófica, religiosa o sindical, siempre que se refieran exclusivamente a sus miembros o a las personas que mantengan contactos regulares por razón de su finalidad. En estos eventos, los datos no se podrán suministrar a terceros sin la autorización del titular.</li>
						<li>El tratamiento se refiera a datos que sean necesarios para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial.</li>
						<li>El tratamiento tenga una finalidad histórica, estadística o científica. En este evento deberán adoptarse las medidas conducentes a la supresión de identidad de los titulares.</li>
						<li>Igualmente, el tratamiento de datos personales de niños, niñas y adolescentes se realizará bajo los parámetros enunciados anteriormente en esta política.</li>
					</ol>

					<h3>I. Legitimación para el ejercicio de los derechos del titular</h3>

					<p>Los derechos de los Titulares establecidos en la Ley, podrán ejercerse por las siguientes personas:</p>
					<ol>
						<li>Por el Titular, quien deberá acreditar su identidad en forma suficiente por los distintos medios que le ponga a disposición el responsable.</li>
						<li>Por sus causahabientes, quienes deberán acreditar tal calidad.</li>
						<li>Por el representante y/o apoderado del Titular, previa acreditación de la representación o apoderamiento.</li>
						<li>Por estipulación a favor de otro o para otro.</li>
					</ol>

					<p><b>Nota:</b> Los derechos de los niños, niñas o adolescentes se ejercerán por las personas que estén facultadas para representarlos.</p>

					<h3>J. Responsables y canales</h3>
					<p align="justify"> la <b>Fundación Rootdevel Hackerspace</b>, con el fin de garantizar el derecho de acceso a los titulares de los datos personales, para la atención de consultas, reclamos, peticiones de rectificación, actualización y supresión de datos, cuenta mecanismos físicos y electrónicos para la recepción de solicitudes, por medio del correo electrónico info@rootdevel.org, los formularios de comunicación en el sitio web https://rootdevel.org o físicamente a través de oficios radicados directamente en las instalaciones de la fundación.</p>

					<h3>K. Autorización del titular</h3>
					<p align="justify"> la <b>Fundación Rootdevel Hackerspace</b>, para el tratamiento de datos personales requiere la autorización previa, expresa e informada del titular de los mismos, excepto en los siguientes casos autorizados por la ley 1581 de 2012:</p>
					<ol>
					<li>Información requerida por una entidad pública o administrativa en ejercicio de sus funciones legales o por orden judicial.</li>
					<li>Datos de naturaleza pública.</li>
					<li>Casos de urgencia médica o sanitaria.</li>
					<li>Tratamiento de información autorizado por la ley para fines históricos, estadísticos o científicos.</li>
					<li>Datos relacionados con el Registro Civil de las Personas.</li>
					</ol>
					<p align="justify">De la autorización requerida para el tratamiento de datos personales sensibles, cuando dicho tratamiento sea permitido, la <b>Fundación Rootdevel Hackerspace</b> deberá cumplir con las siguientes obligaciones:
					<ol>
					<li>Informar al titular que por tratarse de datos sensibles no está obligado a autorizar su tratamiento.</li>
					<li>Informar al titular de forma explícita y previa, además de los requisitos generales de la autorización para la recolección de cualquier tipo de dato personal, cuáles de los datos que serán objeto de tratamiento son sensibles y la finalidad del tratamiento, así como obtener su consentimiento expreso.</li>
					<li>Ninguna actividad podrá condicionarse a que el titular suministre datos personales sensibles.</li>
					<li>Medios para obtener y otorgar la autorización.</li>
					</ol>
						<p align="justify"> la <b>Fundación Rootdevel Hackerspace</b> con el fin de dar cumplimiento a lo establecido en la Ley 1581 de 2012 obtendrá de manera previa al tratamiento de los datos personales, la autorización de los titulares o de quienes se encuentren legitimados para ello a través de diferentes mecanismos como: Formato de autorización para la recolección y tratamiento de datos personales, correo electrónico, página web, mensaje de datos, Intranet o cualquier otro mecanismo que permita concluir inequívocamente que la autorización fue otorgada.<p>
						<p>En ningún caso, el silencio podrá asimilarse a una conducta inequívoca.</p>

						<h3>L. Prueba de la autorización</h3>
						<p align="justify">la <b>Fundación Rootdevel Hackerspace</b>, desplegará los medios físicos y electrónicos necesarios para la conservación de la prueba de autorización otorgada por los titulares de los datos personales para el tratamiento de los mismos sin importar cuál haya sido el medio a través del cual dicha autorización haya sido obtenida.</p>

						<h3>M. Revocatoria de la autorización y/o supresión del dato</h3>
						<p align="justify">Los titulares de los datos personales podrán en todo momento solicitar a  la <b>Fundación Rootdevel Hackerspace</b>, la supresión de sus datos personales y/o revocar total o parcialmente la autorización otorgada para el tratamiento de los mismos, mediante la presentación de un reclamo.</p>
						<p align="justify">La solicitud de supresión de la información y la revocatoria de la autorización no procederán cuando el titular de los datos personales tenga un deber legal o contractual de permanecer en la base de datos.</p>
						<p align="justify">la <b>Fundación Rootdevel Hackerspace</b>, pondrá a disposición del titular de los datos personales mecanismos gratuitos y de fácil acceso para presentar la solicitud de supresión de datos o la revocatoria de la autorización otorgada.</p>

						<h3>N. Medidas de seguridad</h3>

						<p align="justify">la <b>Fundación Rootdevel Hackerspace</b>, a través de su política de Seguridad de la Información y capacitación de personal buscará garantizar el cumplimiento del principio de seguridad de la Ley 1581 de 2012 y demás normas concordantes y complementarias, así como la implementación de la obligación contractual en las relaciones que se adquieran con proveedores y contratistas, que de una u otra forma presten bienes y servicios encaminados a contribuir a la prestación de servicios.</p>

						<h3>N.1. Personas a las cuales se les puede suministrar la información</h3>

						<p align="justify">La información que reúna las condiciones establecidas en la presente política podrá ser suministrada por  la <b>Fundación Rootdevel Hackerspace</b>, a las siguientes personas:</p>
							<ol>
							<li>A los titulares, sus causahabientes o sus representantes legales.</li>
							<li>A las entidades públicas o administrativas en ejercicio de sus funciones legales o por orden judicial.</li>
							<li>A los terceros autorizados por el Titular o por la ley.</li>
							</ol>
						<h3>N.2. Transferencias y transmisiones internacionales de datos personales</h3>

						<p>Para la transmisión y transferencia de datos personales, se aplicarán las siguientes reglas:</p>
						<ol>
							<li>Las transferencias internacionales de datos personales deberán observar lo previsto en el artículo 26 de la Ley 1581 de 2012; es decir, la prohibición de transferencia de datos personales a países que no proporcionen niveles adecuados de protección de datos y los casos excepcionales en los que dicha prohibición no aplica.</li>
							<li>Las transmisiones internacionales de datos personales que se efectúen entre un responsable y un encargado para permitir que el encargado realice el tratamiento por cuenta del responsable, no requerirán ser informadas al titular ni contar con su consentimiento cuando exista un contrato en los términos del artículo 25 de la Ley 1581 de 2012.</li>
							<li>Se prohíbe la transferencia de datos personales de cualquier tipo a países que no proporcionen niveles adecuados de protección de datos. Se entiende que un país ofrece un nivel adecuado de protección de datos cuando cumpla con los estándares fijados por la Superintendencia de Industria y Comercio.</li>
						</ol>
						<p>De manera excepcional,  la <b>Fundación Rootdevel Hackerspace</b>, podrá hacer transferencia de datos personales en los siguientes casos:</p>
						<ol>
						<li>Información respecto de la cual el titular haya otorgado su autorización expresa e inequívoca para la transferencia.</li>
						<li>Intercambio de datos de carácter médico, cuando así lo exija el tratamiento del titular por razones de salud o higiene pública.</li>
						<li>Transferencias bancarias o bursátiles, conforme a la legislación que les resulte aplicable.</li>
						<li>Transferencias acordadas en el marco de tratados internacionales en los cuales la República de Colombia sea parte, con fundamento en el principio de reciprocidad.</li>
						<li>Transferencias necesarias para la ejecución de un contrato entre el titular y  la <b>Fundación Rootdevel Hackerspace</b> o para la ejecución de medidas precontractuales siempre y cuando se cuente con la autorización del titular. Transferencias legalmente exigidas para la salvaguardia del interés público, o para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial.</li>
						</ol>

                        <h3> O. Deberes de los responsables y de los encargados del tratamiento de datos personales</h3>
                       <h3> O.1. Deberes de los responsables del tratamiento de datos personales</h3>
                        <p align="justify">Responsable del Tratamiento: Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, decida sobre la base de datos y/o el Tratamiento de los datos;</p>
                        <p align="justify">El Responsable del Tratamiento, al momento de solicitar al Titular la autorización, deberá informarle de manera clara y expresa lo siguiente:</p>
                        <ol>
                            <li>Tratamiento al cual serán sometidos sus datos personales y la finalidad del mismo;</li>
                            <li>El carácter facultativo de la respuesta a las preguntas que le sean hechas, cuando estas versen sobre datos sensibles o sobre los datos de las niñas, niños y adolescentes;</li>
                            <li>Los derechos que le asisten como Titular;</li>
                            <li>La identificación, dirección física o electrónica y teléfono del Responsable del Tratamiento.</li>
							<li>Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data;</li>
                            <li>Solicitar y conservar, en las condiciones previstas en la presente ley, copia de la respectiva autorización otorgada por el Titular;</li>
                            <li>Informar debidamente al Titular sobre la finalidad de la recolección y los derechos que le asisten por virtud de la autorización otorgada.</li>
                            <li>Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento;</li>
                            <li>Garantizar que la información que se suministre al Encargado del Tratamiento sea veraz, completa, exacta, actualizada, comprobable y comprensible;</li>
                            <li>Actualizar la información, comunicando de forma oportuna al Encargado del Tratamiento, todas las novedades respecto de los datos que previamente le haya suministrado y adoptar las demás medidas necesarias para que la información suministrada a este se mantenga actualizada;</li>
                            <li>Rectificar la información cuando sea incorrecta y comunicar lo pertinente al Encargado del Tratamiento;</li>
                            <li>Suministrar al Encargado del Tratamiento, según el caso, únicamente datos cuyo Tratamiento este previamente autorizado de conformidad con lo previsto en la presente ley;</li>
                            <li>Exigir al Encargado del Tratamiento en todo momento, el respeto a las condiciones de seguridad y privacidad de la información del Titular;</li>
                            <li>Tramitar las consultas y reclamos formulados en los términos señalados en la presente ley;</li>
                            <li>Adoptar un manual interno de políticas y procedimientos para garantizar el adecuado cumplimiento de la presente ley y en especial, para la atención de consultas y reclamos;</li>
                            <li>Informar al Encargado del Tratamiento cuando determinada información se encuentra en discusión por parte del Titular, una vez se haya presentado la reclamación y no haya finalizado el trámite respectivo;</li>
                            <li>Informar a solicitud del Titular sobre el uso dado a sus datos;</li>
                            <li>Informar a la autoridad de protección de datos cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los Titulares.</li>
                            <li>Cumplir las instrucciones y requerimientos que imparta la Superintendencia de Industria y Comercio.</li>
                            <li>Deber de acreditar puesta a disposición del aviso de privacidad y las políticas de Tratamiento de la información, cuando sea usado este medio.</li>
                            <li>Debe conservar el modelo del Aviso de Privacidad que utilicen para cumplir con el deber que tienen de dar a conocer a los Titulares la existencia de políticas del tratamiento de la información y la forma de acceder a las mismas, mientras se traten datos personales conforme al mismo y perduren las obligaciones que de este se deriven.</li>
                            <li>Debe conservar prueba de la autorización otorgada por los Titulares de datos personales para el Tratamiento de los mismos.</li>
                       	   </ol>
						   <p><b>Nota:</b> El Responsable del Tratamiento deberá conservar prueba del cumplimiento de lo previsto en este numeral y, cuando el Titular lo solicite, entregarle copia de esta.</p>

						<h3>O.2. Deberes de los encargados del tratamiento de datos personales</h3>

                       <p align="justify"> Encargado del Tratamiento: Persona natural o jurídica, pública o privada, que por sí misma o en asocio con otros, realice el Tratamiento de datos personales por cuenta del Responsable del Tratamiento;</p>
                        <ol>
                          <li>Garantizar al Titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data;</li>
                          <li>Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento;</li>
                          <li> Realizar oportunamente la actualización, rectificación o supresión de los datos en los términos de la presente ley;</li>
                          <li>Actualizar la información reportada por los Responsables del Tratamiento dentro de los cinco (5) días hábiles contados a partir de su recibo;</li>
                          <li>Tramitar las consultas y los reclamos formulados por los Titulares en los términos, señalados en la presente ley;</li>
                          <li>Adoptar un manual interno de políticas y procedimientos para garantizar el adecuado cumplimiento de la presente ley y, en especial, para la atención de consultas y reclamos por parte de los Titulares;</li>
                          <li>Registrar en la base de datos las leyenda “reclamo en trámite” en la forma en que se regula en la presente ley;</li>
                          <li>Insertar en la base de datos la leyenda “información en discusión judicial” una vez notificado por parte de la autoridad competente sobre procesos judiciales relacionados con la calidad del dato personal;</li>
                         <li>Abstenerse de circular información que esté siendo controvertida por el Titular y cuyo bloqueo haya sido ordenado por la Superintendencia de Industria y Comercio;</li>
                         <li>Permitir el acceso a la información únicamente a las personas que pueden tener acceso a ella;</li>
                         <li>Informar a la Superintendencia de Industria y Comercio cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los Titulares;</li>
                         <li>Cumplir las instrucciones y requerimientos que imparta la Superintendencia de Industria y Comercio.</li>
                        </ol>
                           <p align="justify"><b>Nota 1:</b> en el evento en que concurran las calidades de Responsable del Tratamiento y Encargado del Tratamiento en la misma persona, le será exigible el cumplimiento de los deberes previstos para cada uno.</p>

                            <p><b>Nota 2:</b> Las políticas de Tratamiento en ningún caso podrán ser inferiores a los deberes contenidos en la presente ley.</p>

                        <h3>O.3. Deberes tanto de los responsables como de los encargados del tratamiento de datos personales</h3>
                        <ol>
                           <li>Deben establecer mecanismos sencillos y ágiles que se encuentren permanentemente disponibles a los Titulares con el fin de que estos puedan acceder a los datos personales que estén bajo el control de aquellos y ejercer sus derechos sobre los mismos.</li>
                            <li>Deberán adoptarse las medidas razonables para asegurar que los datos personales que reposan en las bases de datos sean precisos y suficientes y, cuando así lo solicite el Titular o cuando el Responsable haya podido advertirlo, sean actualizados, rectificados o suprimidos, de tal manera que satisfagan los propósitos del tratamiento.</li>
                            <li>Deberán designar a una persona o área que asuma la función de protección de datos personales, que dará trámite a las solicitudes de los Titulares, para el ejercicio de los derechos a que se refiere la Ley 1581 de 2012 y el Decreto 1377 de 2013.</li>
                        </ol>

							<h3>P. Temporalidad del tratamiento de datos personales</h3>
							 <p align="justify">La permanencia de los datos personales recolectados por  la <b>Fundación Rootdevel Hackerspace</b>, estará determinada por la finalidad del tratamiento para los que estos hayan sido recogidos. Una vez cumplida la finalidad del tratamiento, la <b>Fundación Rootdevel Hackerspace</b> procederá a la supresión de los datos personales recolectados. No obstante lo anterior, los datos personales deberán ser conservados cuando así se requiera para el cumplimiento de una obligación legal o contractual.</p>

							<h3>Q. Aviso de privacidad</h3>
							 <p align="justify">En el evento en el que la <b>Fundación Rootdevel Hackerspace</b>, no pueda poner a disposición del titular del dato personal la presente política de tratamiento de la información, publicará el aviso de privacidad que se adjunta al presente documento, cuyo texto conservará para consulta posterior por parte del titular del dato y/o de la Superintendencia de Industria y Comercio.</p>

							<h3>R. Datos recolectados antes de la expedición del decreto 1377 de 2013</h3>
							 <p align="justify">De conformidad con lo dispuesto en el numeral 3 del artículo 10 del Decreto Reglamentario 1377 de 2013 la <b>Fundación Rootdevel Hackerspace</b>, procederá a publicar un aviso en su página web oficial https://rootdevel.org dirigido a los titulares de datos personales para efectos de dar a conocer la presente política de tratamiento de información y el modo de ejercer sus derechos como titulares de datos personales alojados en las bases de datos de la <b>Fundación Rootdevel Hackerspace</b>.</p>

							<h3>S. Fecha de entrada en vigencia</h3>
							 <p align="justify">La presente Política de Protección de Datos Personales rige a partir del miércoles, 24 de Enero de 2018 y deja sin efecto alguno cualquier documento anterior que buscara regular el tema en el interior de la <b>Fundación Rootdevel Hackerspace</b>.</p>

                           <p align="justify"> Si te gustó nuestra política de privacidad y quieres adaptarla a tu sitio web, recuerda que éste, al igual que la mayoría de nuestros contenidos, están licenciados bajo Creative Commons (Atribución-Compartir Igual). Encuentra más información sobre esta licencia en:  <a href="http://creativecommons.org/licenses/by-sa/2.5/co/"target="_blank" rel="noopener">http://creativecommons.org/licenses/by-sa/2.5/co/ </a></p>
                           <p><b>Fuente:</b> <a href="https://flisolbogota.org/aviso-de-privacidad-y-tratamiento-de-datos-personales/" target="_blank">Política de privacidad y tratamiento de datos personales</a> por flisolbogota licenciado bajo CC BY-SA 2.5 CO
                           </p>


				</section>

		</div>
	</div>
