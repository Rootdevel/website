<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Normas de Convivencia</h1>
						<h3>Council Rootdevel Hackerspace (foundation)February 13, 2018</h3>
					</header>

          <hr />

					<!--<a href="#" class="image main"><img src="<?= base_url('/media/images/') ?>" alt="" /></a>_-->


<h2>¡Estamos muy contentos que te hayas vinculado a la comunidad!</h2>
<p>Rootdevel Hackerspace es una entidad sin ánimo de lucro, fundada en el año 2013 en la cuidad de Sogamoso, integrada por un grupo interdisciplinar de personas que dedican sus esfuerzos a compartir conocimiento libre en las diferentes áreas del saber. Cuyas acciones se fundamentan en la amistad, pasión, amor, persistencia, lealtad, honestidad y servicio a la comunidad, permitiendo que la curiosidad por la investigación haga volar la imaginación, para hacer realidad las ideas.</p>
<p>Aquí puedes aprender y resolver inquietudes, para eso está conformada esta comunidad. Si tienes alguna duda sobre: programación, diseño, arte, literatura, idiomas, software y hardware libre … entre otros, hay integrantes que pueden orientarte en estas distintas disciplinas, a quienes puedes acercarte y pedirles que te colaboren. Ellos hacen parte de este gran equipo de trabajo, porque les gusta compartir y generar conocimiento, desarrollar proyectos y actividades con la comunidad; todo esto en un entorno de fraternidad.</p>

<p>Cada uno de nosotros ayuda a que se mantenga este grupo. Te dejamos los siguientes puntos claves para que puedas aprovechar bien este espacio:<p>
<ol>
	<li>Ten claro por qué participas del grupo, no desvirtúes su razón de ser.</li>
	<li>No toques temas personales, particulares o que puedan generar algun tipo de conflicto o malestar entre los participantes.</li>
	<li>Escribe de forma privada un comentario si es necesario o la situacion lo requiere, evita hacerlo publicamente.</li>
	<li>Al participar, hazlo aportando en temas relacionados con la razón de ser del grupo.</li>
	<li>No es necesario contestar absolutamente todo lo que escriban con emoticones o frases repetidas.</li>
	<li>No toques temas que puedan atentar contra la moral o creencias de otros participantes del grupo.</li>
	<li>Si alguno de los integrantes del grupo agrede o ataca al resto, evita ampliar la discusión.</li>
	<li>Procura publicar temas de interés común en el grupo. no coloques publicidad no deseada o no solicitada.</li>
	<li>Se breve, no escribas o publiques testamentos que incomodan al resto de los participantes.</li>
	<li>Recuerda, es molesto que el teléfono suene cada instante, alguien del grupo puede estar en reunión o en el trabajo. Silencia las notificaciones.</li>
</ol>
    <blockquote>"Comparte, disfruta y colabora, son muy buenas prácticas, estamos aquí para aprender y mejorar".</blockquote>

    <blockquote>"Se razonable y respetuoso, no molestes a los demás, no hagas a los demás lo que no te gustaría que te hicieran a ti".</blockquote>

    <blockquote>"La inclusividad es vital para el Hackerspace, todo el mundo es bienvenido".</blockquote>

<h3>Aspectos legales, se prohíbe:</h3>
<ol>
    <li>Spam, insultar o irrespetar.</li>
     <li>Pornografía (contenido insinuante, pornografía infantil, serán notificados a las autoridades correspondientes).</li>
     <li>Ilegalidad​​​​​​​.</li>
     <li>Agregar Bots o jugar con los Bots del gupo.</li>
    <li>En caso de que algún usuario este infringiendo las normas o faltando al respeto, dar avizo a cualquiera de los administradores.</li>
    <li>Hacking, Cracking, etc: El grupo no es para ayudar a realizar ilegalidades, programar scripts de dudoso uso o actos de cualquier tipo que estén fuera de ayudar a instalar, configurar y reparar sistemas GNU/Linux.</li>
    <li>Métodos para obtener “Internet gratis”: Cualquier petición de ayuda para llevar a cabo un robo de internet, tanto de WiFi como de datos móviles, sea esto con HTTP Injection, VPS, Claves, Aircrack o cualquier otra herramienta.</li>
    <li>Material con Copyright o licencias que limiten compartirlo: Telegram prohíbe Copyrigth y eso pone en riesgo, el grupo de ser denunciado por eso el grupo debe mantener la legalidad en todas las publicaciones realizadas en este. Evitar subir este tipo de materiales directamente a Telegram o a sitios de descarga masiva como Mega. Apoyamos la libertad de expresión y de compartir la información, pero no el romper las leyes que protegen este tipo de materiales, por lo que estos deberán ser evitados.</li>
    <li>Rootdevel Hackerspace, no se hace responsable de los daños o perjuicios directos o indirectos derivados del uso del material e información suministrada por la organización.</li>
    <li>El incumplimiento reiterado de estas normas, dará lugar a la expulsión o sanción del integrante, según la gravedad.</li>
     <li>Este espacio, puede ofrecer información sensible, Rootdevel Hackerspace, no asume ninguna responsabilidad derivada de las acciones realizadas por terceros. Y podrá ejercer todas las acciones civiles o penales que correspondan, en caso de infracción de estos derechos por parte del integrante.</li>
</ol>
<h2>Nuestros medios de Comunicación</h2>

<p>Jitsi, es una aplicación de videoconferencia, VoIP, brindan comunicación cifrada en tiempo real. Al ser multiplataforma, es una herramienta muy usada, pero sobre todo por activistas y personas muy dedicadas a proteger su seguridad en línea. Distribuida bajo los términos de la GNU Lesser General Public License.</p>

<h2>Puedes usar Jitsi sin instalar nada</h2>

<p>Es posible usar Jitsi directamente desde una pestaña del navegador, sin iniciar sesión y sin instalar nada adicional. Simplemente escribes http://meet.jit.si en la barra del navegador y ya estás dentro. No necesitas ni siquiera iniciar sesión en ningún servicio; apenas autorizar el uso de tu micrófono y tu videocámara y listo.</p>

<p>Ingresa al siguiente enlace  <a href="https://meet.jit.si/rootdevel">https://meet.jit.si/rootdevel</a></p>

<h2>plataforma de desarrollo colaborativo</h2>

<p>No dudes en colaborar en los distintos proyectos que tenemos en GitHub; por si no lo sabes es una plataforma ideal para el trabajo colaborativo sea cual sea el proyecto.</p>

<p> Ingresa al siguiente enlace <a href="https://gitlab.com/Rootdevel">https://gitlab.com/Rootdevel</a></p>

<h2>Licencias del grupo</h2>

<p>Todo el texto escrito (Mensajes) en este grupo, mientras no sea código fuente o scripts o tenga alguna otra licencia, tendrá automáticamente la licencia libre <a href="https://fdl-es.github.io/FDL_es/">FDL 1.3+</a>. Todo el código fuente o scripts sin licencia establecida por el autor, tendrá de manera automática la licencia <a href="https://lslspanish.github.io/translation_GPLv3_to_spanish/">GPLv3+</a> por defecto.</p>

				</section>

		</div>
	</div>
