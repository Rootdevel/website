<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Wrapper -->
<div class="wrapper">
	<div class="inner">
		<!-- Main -->
		<section class="main">
		<header class="major">
		<h1>Reforma Tributaria Estructural, Ley 1819 del 2016</h1>
		</header>
		<hr/>
		<a href="#" class="image main"><img src="<?=base_url('/media/images/law-1819-of-2016.jpg') ?>" alt="" /></a>
		<p>
			Se estableció una nueva responsabilidad para las entidades clasificadas dentro del Régimen Tributario Especial (RTE), <a href="http://www.secretariasenado.gov.co/senado/basedoc/ley_1819_2016.html" target="_blank">Ley 1819 del 2016</a>. Dichas entidades deben actualizar cada año, en los primeros 3 meses, la información a la que se refiere el parágrafo 2 del artículo 364-5 del Estatuto Tributario.
		</p>
		<p>
			La información debe ser publicada en la página web de la entidad y como mínimo debe contener lo siguiente:
		</p>
		<ol>
			<li>DENOMINACION, IDENTIFICACION Y DOMICILIO</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/1. DENOMINACION, IDENTIFICACION Y DOMICILIO.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>DESCRIPCION ACTIVIDAD</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/2. DESCRIPCION ACTIVIDAD.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>MONTO Y DESTINO DE LA REINVERSION DEL BENEFICIO O EXCEDENTE NETO</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/3. MONTO Y DESTINO DE LA REINVERSION DEL BENEFICIO O EXCEDENTE NETO.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>MONTO Y DESTINO DE LAS ASIGNACIONES PERMANENTES</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/4. MONTO Y DESTINO DE LAS ASIGNACIONES PERMANENTES.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>NOMBRES E IDENTIFICACION DE LAS PERSONAS QUE OCUPAN CARGOS GERENCIALES</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/5. NOMBRES E IDENTIFICACION DE LAS PERSONAS QUE OCUPAN CARGOS GERENCIALES.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>MONTO TOTAL DE PAGOS SALARIALES</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/6. MONTO TOTAL DE PAGOS SALARIALES.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>NOMBRES IDENTIFICACION DE LOS FUNDADORES</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/7. NOMBRES IDENTIFICACION DE LOS FUNDADORES.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>MONTO DEL PATRIMONIO DEL AÑO ANTERIOR</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/8. MONTO DEL PATRIMONIO DEL AÑO ANTERIOR.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>DONACIONES</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/9. DONACIONES.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>DONACIONES EN EVENTOS COLECTIVOS</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/10. DONACIONES EN EVENTOS COLECTIVOS.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>INFORME DE GESTION</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/11. INFORME DE GESTION.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>ESTADOS FINANCIEROS DE LA ENTIDAD</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/12. ESTADOS FINANCIEROS DE LA ENTIDAD.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>CERTIFICACION NUMERAL 13, ARTICULO 364-5 ET</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/13.') ?>">Descargar Documento</a>
				</p>
			</ul>
			<li>RECURSOS DE COOPERACION INTERNACIONAL</li>
			<ul>
				<p>
					<a href="<?=base_url('/media/doc/14. RECURSOS DE COOPERACION INTERNACIONAL.pdf') ?>">Descargar Documento</a>
				</p>
			</ul>
		</ol>
			</section>

	</div>
	</div>
