<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<body>

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Wrapper -->
					<div class="wrapper">
						<div class="inner">

							<?php $this->load->view('header');?>
							<?php $this->load->view('nav');?>
							<?php $this->load->view($wrapper);?>

							<!-- Wrapper -->
              <div class="wrapper">
                <div class="inner">

                  <?php $this->load->view('footer');?>

                </div>
              </div>


					</div>

				<!-- Scripts -->
					<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
					<script src="<?= base_url() ?>assets/js/util.js"></script>
					<script src="<?= base_url() ?>assets/js/skel.min.js"></script>
					<script src="<?= base_url() ?>assets/js/main.js"></script>

	</body>
