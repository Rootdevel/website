<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>MicroPython + Wifi con el Modulo ESP8266 por 4 dolares</h1>
					</header>

          <hr />

					<a class="image main"><img src="<?= base_url('media/images/micropython/NodeMCU_DEVKIT_1.0.jpg') ?>" alt="" /></a>

					<blockquote>Me ha llegado mi modulo Wifi + plataforma de desarrollo ensamblada por Node MCU que no me a salido por mas de 4 dolares (USD). Desde china hasta mi casa.</blockquote>

					<p align="justify">Este kit de desarrollo (NodeMCU) ya cuenta con unidad conversora de RS-232 (TTL) a USB con su respectivo regulador 3.3 Voltios con lo cual permite conectar éste directamente al puerto USB de nuestra computadora y programarlo o cargar el firmware y desde luego el modulo Wifi ESP8266 que  está basado en el Modulo EAP-12E que cuenta con 22 Pines y mas que ya podremos apreciar en las siguientes imágenes:</p>


				    <center>
					<h3>Modulo Wifi ESP8266 (EAP-12E)</h3>
					<div class="6u"><span class="image fit"><img src="<?= base_url('media/images/micropython/1438017644051-P-2850611.jpg') ?>" alt="Modulo Wifi ESP8266 (EAP-12E)" data-position="center"></span></div>
					<h3>Kit de Desarrollo NodeMCU - ESP8266 (ESP-12E) </h3>
					<div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/ESP-12E-Placa_de_desarrollo.png') ?>" alt="" data-position="fixed"></span></div>
                    </center>

                    <center><h2>El modulo Node MCU</h2> </center>
                    <div class="video">
                	<iframe src="https://www.youtube.com/embed/FWQ7D8zzYnk" frameborder="0" allowfullscreen></iframe>
                   	</div>
                    <center><h3> Mas info  <a href="https://www.esp8266.com/wiki/doku.php?id=start"target="_blank" rel="noopener"> ESP8266 </a></h3> </center>

					<p align="justify">Lo he comprado por su precio y funcionalidad además que su tamaño me encanta y pretendo embeberle MycroPython como firmware que para quienes no saben para mucho y ahí me incluyo MicroPython es el futuro de los lenguajes embebidos.</p>

                     <h2>FIRMWARE -  <a href="http://micropython.org/"target="_blank" rel="noopener">MicroPython</a></h2>

                    <p align="justify">Para contextualizar un poco o si tienes te surgen  dudas o algo de curiosidad MicroPython es: Micro Python es una implementación del lenguaje de programación Python 3 optimizado para microcontroladores [1].</p>
                    <p align="justify">Otro de los grandes proyectos financiados por crowdfunding <a href="https://www.kickstarter.com/projects/214379695/micro-python-python-for-microcontrollers?ref=video"target="_blank" rel="noopener">Micro Python: Python for microcontrollers</a> creado por DamienGeorge</p>
				    <div class="video">
                	<iframe src="https://www.youtube.com/embed/5LbgyDmRu9s" frameborder="0" allowfullscreen></iframe>
                	</div>

                    <p align="justify">Puedes probar MicroPython de manera virtual corriendo en la pybord y testeando una serie de periféricos: <a href="http://micropython.org/live/"target="_blank" rel="noopener"><h2>http://micropython.org/live</h2></a></p>
                    <p align="justify"> Ahora lo genial es que puedes tener MicroPython en el modulo Wifi que eh citado anteriormente.</p>
                    <p align="justify">Gracias a los chic@s de  <a href="https://www.adafruit.com/"target="_blank" rel="noopener">Adafruit</a> tenemos una grandiosa guía en la cual eh basado parte de este post.</p>
                    <p align="justify"> Para embeber MicroPython en el modulo Wifi ESP8266 existen dos manera de hacerlo, La forma rápida y la manera correcta en donde compilamos desde el código fuente la ultima versión de MicroPython, sin dejar de lado que podemos realizar modificaciones aportando nuevas características que se han implementado en las ultimas revisiones pues hay día de hoy es un proyecto reciente.</p>

                    <h2>INSTALANDO firmware MicroPython funcional</h2>
                    <p align="justify">Primero hemos de probar nuestro modulo, verificando que todo esta bien y que podemos cargar un firmware (Micropython) de manera correcta y funcional. Bastara con descargar, descomprimir y cargar el ultimo firmware compilado <b>"firmware-combined.bin"</b> debemos recordar que este firmware que cargamos de esta manera se encuentra desactualizado, no posee características o modificaciones a medida y que éste no soportara las ultimas funcionalidades de MycroPython.</p>
                    <p align="justify">Para cargar el firmware usaremos en script en Python <a href="https://github.com/espressif/esptool"target="_blank" rel="noopener"> esptool.py</a>, desde luego en cualquier plataforma sea GNU/Linux windows o Mac OS, desde sistemas windows se puede usar la herramienta con interfaz gráfica <a href="https://github.com/nodemcu/nodemcu-flasher"target="_blank" rel="noopener">nodemcu-flasher.</a></p>

                    <a href="https://github.com/nodemcu" target="_blank" rel="noopener"><h2>https://github.com/nodemcu</h2></a>

                   <p> Si usamos el script escrito en python exptool.py hemos de cumplir algunos requerimientos fundamentales como:</p>

                    <ol>
                        <li>Python 2.7.x</li>
                        <li>La librería pyserial (python-serial) </li>
                        <li>Gestor de versiones GIT.</li>
                    </ol>
                    <p>En sistemas Debian o basados en Debian como Ubuntu usar:</p>
                     <pre><code>$ apt-get install python python-serial git </pre></code>


                    <p align="justify">Descargamos el script esptool.py</p>

                    <pre><code>git clone https://github.com/themadinventor/esptool.git </pre></code>

                    <p align="justify">También podemos solo descargar el script presidiendo de la herramienta de gestión de versiones GIT.</p>

                     <ol>
                    <li>Descargamos el firmware <a href="https://s3.amazonaws.com/adafruit-download/MicroPython_ESP8266_Firmware_5_12_2015.zip"target="_blank" rel="noopener"> MicroPython para el modulo ESP8266</a> <b>“firmware-combined.bin”</b></li>
                    <li>Versiones oficiales: <a href="https://micropython.org/download#esp8266"target="_blank" rel="noopener">https://micropython.org/download#esp8266</a></li>
                   <li> El que yo eh compilado, la versión 1.67xxx firmware - MicroPython v1.7xx [2016/04], <a href="https://www.openmailbox.org/index.php/s/zKvMjo16t49XaDT"target="_blank" rel="noopener"> firmware-combined 2016-Oct</a> u <a href="https://www.openmailbox.org/index.php/s/pvgxrOdpx5iBEvO"target="_blank" rel="noopener"> otros.</a></li>
                   <li> Ultima versión compilada del firmware:  <a href="https://www.openmailbox.org/index.php/s/6wnZ4DY7DmemgDF"target="_blank" rel="noopener"> MicroPython-current_version</a></li>
                     </ol>

                    <p>Hemos de ubicar el <b>firmware-combined.bin</b> en conjunto con el script esptool.py</p>

                    <center>
                    <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/1.ls.png') ?>" alt="" data-position="fixed"></span></div>
                    <h3>Firmware MicroPython & Programa de carga</h3> </center><br>

                     <p>El siguiente paso es conectar nuestro modulo vía USB e identificar el puerto serie emulado. Para lo cual abrimos una terminal y listamos los dispositivos serie </p>

                     <pre><code>$ ls -l /dev/tty*</pre></code>

                    <center>
                    <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/2.tty.png') ?>" alt="" data-position="fixed"></span></div>
                   <h3> Dispositivo Serial - USB</h3>
                    </center><br>

                  <p>En mi kit de desarrollo no en necesario pero para algunos kit de desarrollo o dependiendo el firmware pre-instalado se requiere mantener pulsado el botón flash y darle botón reset o el botón flash  o ambos procedimientos anteriormente descritos para lograr cargar el firmware.</p>
                  <p>Se recomienda antes de cargar cualquier firmware limpiar la flash, esto se realiza mediante:</p>

                <pre><code>$ python esptool.py --port /dev/ttyUSB0 erase_flash</pre></code>

                <p align="justify">Por ultimo procedemos a cargar el firmware, para posteriores cargas del firmware como el que construiremos se aplica el mismo proceso de carga del firmware,obtenemos un resultado similar al siguiente:  </p>

                <pre><code>$ python esptool.py -p /dev/ttyUSB0 --baud 460800 write_flash --flash_size=4m 0 firmware-combined.bin</pre></code>

                 <p>En las versiones mas actualizadas de esptool cargar el firmware mediante:</p>

                <pre><code>$ python esptool.py --port /dev/ttyUSB0 write_flash 0 firmware-combined-current.bin</pre></code>

				<center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/3.flash-fw.png') ?>" alt="" data-position="fixed"></span></div>
				<h3>Flash firmware con MicroPython</h3>
                </center><br>

                <p> Y esto por si requieres volver al firmware Lua por alguna razón:</p>

                <pre><code>$ ./esptool.py --port /dev/ttyUSB0 write_flash --flash_size=4m --flash_mode dio 0x00 ./nodemcu_integer_0.9.5_20150318.bin </pre></code>

                 <p>Si todo ah salido bien saltar a la parte final del post para ver el USO de MicroPython en la ESP8266. Más info:</p>

                <ul>
                <li><a href="http://docs.micropython.org/en/latest/esp8266/index.html"target="_blank" rel="noopener">http://docs.micropython.org/en/latest/esp8266</li>
                <li><a href="http://www.esp8266.com/wiki"target="_blank" rel="noopener">http://www.esp8266.com/wiki</a></li>
                <li><a href="http://www.esp8266.com/wiki/doku.php?id=nodemcu"target="_blank" rel="noopener">http://www.esp8266.com/wiki/doku</a></li>
                </ul>

                <h2>CONSTRUIR EL FIRMWARE</h2>
                <p align="justify"> Nuevamente todo el crédito es para  <a href="https://learn.adafruit.com/users/tdicola"target="_blank" rel="noopener"> Tony DiCola (Adafruit)</a> y sus  <a href="https://learn.adafruit.com/building-and-running-micropython-on-the-esp8266/build-firmware"target="_blank" rel="noopener">grandes aportes.</a><p>
                <p align="justify"> Para construir el firmware MicroPython el método mas sano es realizarlo desde una maquina virtual (Que si lo puedes hacer desde tu sistema nativo), esto porque es mas fácil y portable realizarlo en una maquina virtual además de poder usar el SDK abierto de manera aislada y que no entre en conflicto con las herramientas propias del sistema y sin depender de la plataforma en la que te encuentres (windows, Mac OS, GNU/Linux), una maquina virtual que ejecuta Ubuntu, que ejecuta las herramientas y SDK para compilar el firmware de MicroPython para el procesador de la ESP8266.</p>

                <h3>Dependencias</h3>
                <ul>
                  <li> <a href="https://www.virtualbox.org/"target="_blank" rel="noopener"> VirtualBox</a> - Software de visualización open source o no; <b>se requiere version 4.x</b></li>
                  <li>  <a href="https://www.vagrantup.com/"target="_blank" rel="noopener"> Vagrant</a> -Que nos permite crear y configurar entornos de desarrollo virtualizados desde la grandiosa y poderosa terminal (linea de comandos).</li>
                  <li>  <a href="http://git-scm.com/"target="_blank" rel="noopener"> Git</a> - software de control de versiones que usaremos para descargar la ultima revisión de MicroPython y el SDK.</li>
                </ul>

                <p align="justify"> En Debian y derivados, como en otras distros, están en los repos. Una vez cumplido las dependencias nuestro primer paso sera el de descargar una archivo-vagrant el cual determina que sistema operativo usar, además de algunos comandos que adecuan el sistema operativo para construir el SDK. desde la terminal clone el repositorio.</p>

                <p> Clonar el repositorio, donde se encuentra el archivo vagrant</p>

                <pre><code>$ git clone https://github.com/adafruit/esp8266-micropython-vagrant.git</pre></code>

                <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/4.git clone.png') ?>" alt="" data-position="fixed"></span></div>
                <h3>Clonar Repositorio</h3>
                </center><br>

                <p>Luego entrar al directorio</p>
                <pre><code>$ cd esp8266-micropython-vagrant/</pre></code>

                <p>Ejecutar maquina virtual </p>
                <pre><code>$ vagrant up</pre></code>

                <p> Es normal que tarde un tiempo la primera vez </p>

                <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/5.vagrant.png') ?>" alt="" data-position="fixed"></span></div>
                <h3>vagrant - levantar maquina virtual</h3>
                </center><br>

                <p> Ahora viene lo genial y es que podremos acceder a nuestra maquina virtual en linea de comandos con:</p>
                <pre><code>$ vagrant ssh</pre></code>

				 <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/6.vagrant-ssh.png') ?>" alt="" data-position="fixed"></span></div>
				<h3>vagrant ssh - Acceder a la maquina virtual</h3>
				</center><br>

				<p>	Bueno "manos a la obra" a construir el ESP-SDK</p>
                <p align="justify"> La maquina virtual ya viene preparada para su trabajo, es decir ya tiene el <a href="https://github.com/pfalcon/esp-open-sdk"target="_blank" rel="noopener"> código fuente</a> del SDK, si no lo tiene ó lo requieres pues descárgalo, accede a el y a compilar:</p>
                <pre><code>$ cd ~/esp-open-sdk<br>$ make STANDALONE=y </pre></code>

                <p align="justify">Si has compilado alguna vez, un programa, sabrás que toma tiempo y claro mas si lo haces desde una maquina virtual, si te genera error en Adafruit dicen que puede ser por muy poca memoria RAM en dado caso solo asígnale mas en editando el archivo Vagrantfile.</p>
                <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/7.vagrant-compilar.png') ?>" alt="" data-position="fixed"></span></div>
               <h3> Compilar ESP DSK</h3>
                </center><br>

                 <p>Con esto tendremos el ESP Open SDK listo, el paso siguiente desde luego, es el de compilar el firmware MicroPython pero antes requerimos exportar el directorio del SDK para que este pueda encontrado por MicroPython.</p>

                <pre><code>$ echo "PATH=$(pwd)/xtensa-lx106-elf/bin:\$PATH" >> ~/.profile<br>$ exit<br>$ vagrant ssh <br></pre></code>

                 <p>Con el siguiente comando se verifica que el path este bien: </p>
                 <pre><code>$ echo $PATH <br></pre></code>

                 <p align="justify">Finalmente vamos a construir el firmware de MicroPython para la ESP8266, recuerda que debes tener la maquina virtual en ejecución y el SDK compilado.</p>
                  <p align="justify">Una vez mas ya contamos con el código fuente de MicroPython en la maquina virtual gracias al acondicionamiento que realizo el vagrant. Aunque si vamos a requerir de cumplir unas cuantas dependencias externas, dependencias que en muchos caos se requieren para habilitar funcionalidades adicionales</p>

                 <pre><code>$ cd ~/micropython <br>$ git submodule update --init <br>$ make -C mpy-cross <br>$ cd ~/micropython/esp8266 <br>$ make axtls <br>$ make</pre></code>

                <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/8.compilar+micropython.png') ?>" alt="" data-position="fixed"></span></div>
                <h3>Compilar MicroPython</h3>
                </center><br>

                 <h2> Errores?</h2>
                 <p align="justify"> Con esto, ya tendremos el firmware de MicroPython para el ESP8266, en el archivo ./build/firmware-combined.bin, esté sera  el que carguemos a el modulo wi-fi, como este archivo, se encuentra en una maquina virtual necesitamos extraerlo a host esto lo realizamos con el siguiente comando gracias a que vagrant ya tiene un directorio para compartir archivos entre la maquina virtual y el host.</p>

                 <pre><code>$ cp ./build/firmware-combined.bin /vagrant/</pre></code>

                  <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/8.compilar-micropython-cp.png') ?>" alt="" data-position="fixed"></span></div>
                <h3>Compilar MicroPython</h3>
                </center><br>

                <p>Para cargar el firmware favor volver a la parte de INSTALANDO firmware MicroPython funcional</p>
                <p> Salimos e inhabilitamos la maquina virtual con:</p>
                 <pre><code>$ exit <br>$ vagrant halt</pre></code>

                <p align="justify"> Recuerde que para habilitar la maquina virtual en un futuro, para compilar la una versión mas actualizada de MicroPython  o para agregar nuevas funcionalidades usar: </p>
                 <pre><code>$ vagrant up </pre></code>

                <p> Y para acceder a esta: </p>
                <pre><code>$ vagrant ssh </pre></code>

                <h2>USO</h2>
                <p align="justify"> Para usarlo en modo prompt REPL (stands for Read Evaluate Print Loop), es un modo de consola interactiva, este modo es la manera mas fácil de poner a prueba nuestro firmware MicroPython que acabamos de construir y cargar en al ESP8266. Basstara con identificar el dispositivo USB serial (si no sabes como te recomiendo mires en el principio del post - Dispositivo Serial - USB) con:</p>
                <pre><code>$ ls -l /dev/tty*</pre></code>

                 <p>Es muy importante que este puerto se abra a 115200 baudio, en sistemas Unix, GNU/Linux puedes usar minicom, screen, picocom y demas yo usare screen.</p>
                <pre><code>$ screen /dev/ttyUSB0 115200</pre></code>

                 <p align="justify">Recuerda dar un reset a tu Kit de desarrollo una vez terminado, de cargar el firmware y de presionar la tecla Enter por lo menos una vez. Si después reseteamos la placa de desarrollo con el puerto abierto identificaremos al versión de MicroPython cargado en la ESP8266 así como otra información:</p>

                  <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/9.Hola micropython.png') ?>" alt="" data-position="fixed"></span></div>
                 <h3>Hola MicroPython en ESP8266</h3>
                </center><br>

                 <p align="justify"> Si queremos terminar la consola interactiva o cerrar el puerto lo hacemos presionando primero la combinación de teclas CTRL + A y posteriormente presionando la tecla A.</p>
                 <p> Realicemos una serie de pruebas o test en nuestro prompt de MicroPython:</p>

                  <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/10.Hola micropython.png') ?>" alt="" data-position="fixed"></span></div>
                 <h3>Hola Mundo ESP8266 con MicroPython</h3>
                </center><br>

                 <p align="justify">Igualmente podemos realziar este tipo de test sobre un IDE, ESPlorer es un IDE que esta en desarrollo para el modulo Wifi ESP8266, esta escrito en java por lo tanto es multi-plataforma asimismo soporta lenguajes como Lua y MicroPython entre muchas cosas mas.</p>
                <p align="justify"> En el siguiente link pueden descargar el <a href="http://esp8266.ru/esplorer-latest/?f=ESPlorer.zip"target="_blank" rel="noopener"> IDE+firmware MicroPyhon: esp8266.ru</a></p>

                  <center>
                <div class="20u"><span class="image fit"><img src="<?= base_url('media/images/micropython/holaMundo-ESPlorer.png') ?>" alt="" data-position="fixed"></span></div>
                 <h3>Hola Mundo MicroPython (ESP8266) desde el IDE ESPlorer</h3>
                </center><br>

                <p align="justify"> De momento el unico procedimiento de trabajar con scripts .py es guardarlo al momento de compilar el firmware pero no obstante aunque resulte algo tedioso no es difícil ya que solo basta habilitar la maquina virtual (vagrant up), acceder a la maquina (vagrant ssh) y almacenar el script en el directorio</p>
                <pre><code>$ cd ~/micropython/esp8266/scripts/</pre></code>
                <p align="justify"> Recuerda que MicroPython esta totalmente reescrito para estas plataformas, además de ser MicroPython para la ESP8266 muy reciente y por lo tanto no es compatible con todas las bibliotecas de MicroPython. Lo mas sano es siempre fijarse la documentación y tener el firmware actualizado.</p>

                <p align="justify">  <a href="https://docs.micropython.org/en/latest/pyboard/"target="_blank" rel="noopener"> Documentación MicroPython</a> y la referente a las funciones propias de  <a href="https://docs.micropython.org/en/latest/pyboard/library/esp.html"target="_blank" rel="noopener"> ESP.</a></p>

                <h2> OJO MicroPython ESP8266</h2>
               <ul>
                    <li>Aun no sopota mciroSD como los demás tarjetas con MicroPython como la pyboard.</li>
                   <li> No soporte threading o multiprocessing.</li>
                   <li> Micropython no cuenta aun con la biblioteca completa estándar de Python</li>
                </ul>
                 <p>Dejaremos para una próxima entrada la implementación de ejemplos de esta fantástica placa de desarrollo.

                <ul>
                <h3>Referencias Micropython:</h3>
                <li> <a href="http://www.ignogantes.net/micro-python/"target="_blank" rel="noopener">[1] http://www.ignogantes.net/micro-python</li>
                <li> <a href="https://github.com/micropython/micropython/tree/master/esp8266"target="_blank" rel="noopener">https://github.com/micropython/micropython/tree/master/esp8266</li>
                <li> <a href="http://docs.micropython.org/en/latest/esp8266/index.html"target="_blank" rel="noopener">http://docs.micropython.org/en/latest/esp8266/index.html</li>
                <li> <a href="http://docs.micropython.org/en/latest/pyboard/"target="_blank" rel="noopener">http://docs.micropython.org/en/latest/pyboard</li>
                <li> <a href="http://docs.micropython.org/en/latest/esp8266/index.html"target="_blank" rel="noopener">http://docs.micropython.org/en/latest/esp8266/index.html</li>
                <li> <a href="https://www.esp8266.com/wiki"target="_blank" rel="noopener">https://www.esp8266.com/wiki</li>
                <li> <a href="https://www.esp8266.com/wiki/doku.php?id=nodemcu"target="_blank" rel="noopener">https://www.esp8266.com/wiki/doku.php?id=nodemcu</li>
                <li> <a href="https://www.adafruit.com/"target="_blank" rel="noopener">https://www.adafruit.com</li>
                <li> <a href="https://www.virtualbox.org/"target="_blank" rel="noopener">https://www.virtualbox.org</li>
                <li> <a href="https://git-scm.com/"target="_blank" rel="noopener">https://git-scm.com</li>
                <li> <a href="https://www.vagrantup.com/"target="_blank" rel="noopener">https://www.vagrantup.com</li>
                </ul>

                 <div class="3u"><span class="image fit"><img src="<?= base_url('media/images/Yo-Si-Uso-SotwareLibre.png') ?>" alt="" data-position="fixed"></span></div>
                 <p>Lo referente a mi aporte se licencia bajo Creative Common by fandres. </br>Fuente: <a href="https://cc-electrognu.blogspot.com.co/2016/05/ESP8266-micropython.html"target="_blank" rel="noopener"> MicroPython + Wifi con el Modulo ESP8266 por 4 dolares por fandres licenciado bajo  <a href="https://creativecommons.org/licenses/by/4.0/"target="_blank" rel="noopener">CC BY 4.0</a></p>

                </div>
				</section>

		</div>
	</div>
