<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Hackerspaces: refugios de creatividad y conocimiento</h1>
						<p>"Los hackerspaces surgieron como lugares físicos en los que se reúne gente con intereses comunes, casi siempre afines a la tecnología, la ciencia, la informática y el arte digital, aunque tampoco están cerrados a otros tipos de expresiones."</p>
					</header>

          <hr />

					<a class="image main"><img src="<?= base_url('media/images/hackerspaces/hackerspaces_rootdevel.jpg') ?>" alt="" /></a>

                    <p align="justify"> Los hackerspaces dan nacimiento a muchas ideas y son semilleros de cientos de invenciones sólo por el gusto de hacerlas (o por los lulz). Se organizan talleres, charlas y mesas de trabajo, así como algunas reuniones más animadas como jam sessions. Además, se dispone de muchísimo material para laborar. Una de las cosas que distingue a los hackerspaces es que siempre encontrarás cosas para modificar o construir. Las herramientas --desde libros y manuales hasta artefactos y cables-- son parte central del concepto.</p>

                    <div  class="video">
                    		<iframe src="https://www.youtube.com/embed/vkExB1rPnzQ" frameborder="0" allowfullscreen></iframe>
                    </div>

                    <p align="justify"> El hackerspace funciona como una comunidad en la que un grupo de personas renta un lugar (una casa, un edificio viejo) para modificarlo. Entre todos se reparten los gastos de manutención y así sobrevive. Muchas veces, se otorgan membresías que te permiten acceso al lugar a cualquier hora, uso del material y demás. Con ese dinero de sus feligreses, los dirigentes --casi siempre, electos por los miembros-- logran amortizar los gastos y mantener abierto el sitio. Este modelo es contrario al de los hacklabs, que también persiguen el mismo fin, pero se caracterizan por ocupar lugares públicos para darles otra finalidad.</p>
                    <p align="justify"> Los hackerspaces son vistos como simples lugares recreativos. No obstante, estos sitios representan aún una alternativa donde florecen con libertad proyectos enfocados al software libre, el hacking, la invención y la creación artística.</p>

                    <div  class="video">
                    		<iframe src="https://www.youtube.com/embed/wamwklXWK4M" frameborder="0" allowfullscreen></iframe>
                    </div>

                   <h2>¿En dónde encontrarlos?</h2>
                   <p> Si se topan con alguno, anímense a entrar, a apropiarse de él y a difundir la palabra. Vaya que nos hacen falta más sitios así. </p>
                    <span class="image fit"><img src="<?= base_url('media/images/hackerspaces/List_of_Hacker_Spaces.jpg') ?>" alt=""></span>

                     <p align="justify"> Esta es una lista completa y mantenida por el usuario de todos los hackerspaces activos en todo el mundo.</p>
                     <p align="justify"> Actualmente hay 2273 espacios de hackers enumerados en esta wiki, 1411 de ellos están marcados como activos y 351 según lo planeado. También tenemos una lista de espacios de piratería informática planificados, así como una lista de TODOS los espacios de piratería informática en todo el mundo, incluidos los que aún están en proceso de construcción, o que ya están cerrados. Los datos en tiempo real de algunos hackerspaces activos están disponibles en el proyecto Space API.</p>
                     <a href="https://wiki.hackerspaces.org/Hackerspaces" class="button special icon fa-search"> Wiki Hackerspaces</a>
                    <HR>
                    <p align="justify">Si te gustó nuestra entrada y quieres adaptarla a tu sitio web, recuerda que éste, al igual que la mayoría de nuestros contenidos, están licenciados bajo Creative Commons (Atribución-Compartir Igual). Encuentra más información sobre esta licencia en: http://creativecommons.org/licenses/by-sa/2.5/co/</p>

                     <div class="3u"><span class="image fit"><img src="<?= base_url('media/images/Yo-Si-Uso-SotwareLibre.png') ?>" alt="" data-position="fixed"></span></div>
                    <p>Fuente: <a href="https://www.cyberhades.com/2017/11/25/hackspace-magazine/" target="_blank">Cyberhades-HackSpace, Tu Revista Gratuita Sobre Hackerspaces</a> por tuxotron licenciado bajo CC BY-SA 2.5 CO <br>Fuente: <a href="https://hackspace.raspberrypi.org/" target="_blank">HackSpace magazines</a> por Raspberry Pi Trading Ltd. licenciado bajo CC BY-NC-SA 3.0</p>

                    </div>
                    </div>
		    </section>
		</div>
	</div>
