<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>HackSpace magazine, Tu Revista Gratuita Sobre Hackerspaces</h1>
						<p>"HackSpace magazine, es la nueva revista mensula para todas aquellas personas que amán hacer las cosas (DIY) y aquellos que quieren aprender; ¡Coger cinta adhesiva, programar un microcontrolador, preparar una impresora 3D y Hackear el mundo que les rodea!"</p>
					</header>

          <hr />

					<a class="image main"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-3.jpg') ?>" alt="" /></a>


                    <p align="justify"> <a href="https://hackspace.raspberrypi.org"target="_blank" rel="noopener"> HackSpace</a>, una nueva revista gratuita en formato electrónico o de pago (en papel) que pretende promover la cultura de los espacios para “hackers”, o más conocidos como hackerspaces.</p>
                    <p align="justify"> Más allá de la revista en sí, HackSpace quiere formar una comunidad a nivel mundial entorno a este concepto, donde se compartan ideas, proyectos, creación de hackerspaces, etc. El dinero que se recaude a través de HackSpace, será donado a la Fundación Raspberry Pi.</p>
                    <p align="justify">El <a href="https://hackspace.raspberrypi.org/issues/1"target="_blank" rel="noopener">número 1</a> ya está disponible, y lo puedes descargar en formato PDF, a través de <a href="https://itunes.apple.com/us/app/hackspace-magazine/id1315673274?ls=1&mt=8"target="_blank" rel="noopener"> iTunes</a> o <a href="https://play.google.com/store/apps/details?id=com.apazine.hackspace"target="_blank" rel="noopener"> Google Play</a> de forma totalmente gratuita. Y también tienes 3 formas de subscripión, una gratuita para la versión PDF y dos de pago, una mensual y la otra anual (12 meses).</p>

                    	<div  class="video">
                    		<iframe src="https://www.youtube.com/embed/HIsD9sGTa2g" frameborder="0" allowfullscreen></iframe>
                    	</div>

                   <h2>HackSpace magazine #1</h2>
                   <p>December 2017 </p>
                    <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada1.jpg') ?>" alt=""></span>
                    <p align="justify">The first issue of HackSpace magazine is here! Join us as we explore thinking machines, build a trebuchet, learn how Arduino changed the world, see how far we can overclock a Raspberry Pi using liquid nitrogen, and much more…
                    <ul>
                    <li>and much more!</li>
                    <li>Everything you wanted to know about duct tape</li>
                    <li>Build your own Christmas decorations</li>
                    <li>The best DIY portable games consoles</li>
                    <li>Cold-smoke your dinner</li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/007/original/HackSpaceMag01.pdf?1511368038" class="button icon fa-download"> Descargar PDF</a>
                    <HR>
                    <h2>HackSpace magazine #2</h2>
                    <p>January  2018 </p>
                    <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada2.jpg') ?>" alt=""></span>
                      <p align="justify">In HackSpace magazine issue two, we dive into the world of 3D printing, learn how Bristol Hackspace is helping Braille become more accessible, create the best looking goggles around, and uncover the art of the conference badge.</p>
                    <ul>
                       <li> Press your own cheese</li>
                        <li>Design a customised Arduino</li>
                        <li>Laser-cut an LED clock</li>
                        <li>Make your own knife</li>
                        <li>and much more!</li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/008/original/HackSpaceMag02.pdf?1513762934" class="button icon fa-download"> Descargar PDF</a>
                    <HR>
                    <h2>HackSpace magazine #3</h2>
                    <p>February   2018 </p>

                   <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada3.jpg') ?>" alt=""></span>
                      <p align="justify">In HackSpace magazine issue 3, we look at great builds you can make with parts salvaged from broken bits of technology, meet a group of makers working in disaster zones, learn how to 3D print textures onto you clothes and much more:</p>
                    <ul>
                       <li> Learn how to use an oscilloscope</li>
                        <li>Build a drone race track</li>
                        <li>Use silver soldering to build a toolbox</li>
                        <li>Discover the best laser cutting software</li>
                        <li>Find out how to build an automatic chicken door opener</li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/010/original/HackSpaceMag03.pdf?1517222752" class="button icon fa-download"> Descargar PDF</a>
                    <HR>
                     <h2>HackSpace magazine #4</h2>
                    <p>March    2018 </p>
                   <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada4.png') ?>" alt=""></span>
                      <p align="justify">This month in HackSpace magazine, we’re looking at wearable tech from fashionable LEDs to complex cosplay builds. We also chat with Guild of Makers head honcho Dr Lucy Rogers, discover the best displays to add to your electronics and much more: </p>
                    <ul>
                       <li> An internet connected tea machine</li>
                        <li>Paperclip trebuchets</li>
                        <li>Nixie tube countdown</li>
                        <li>Recreating the world’s first stored program computer</li>
                        <li>Monitoring solar power generation </li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/011/original/HackSpaceMag04.pdf?1518787630" class="button icon fa-download"> Descargar PDF</a>
                    <HR>
                          <h2>HackSpace magazine #5</h2>
                    <p>April    2018 </p>
                   <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada5.jpg') ?>" alt=""></span>
                      <p align="justify">In HackSpace magazine issue 5 Limor Fried of Adafruit teaches us what it takes to make great hardware. We also find out everything there is to know about LEDs, convert a Dremel rotary tool into a table saw and much, much more:  </p>
                    <ul>
                       <li> Ruth Amos chats about Kids Invent Stuff and Girls With Drills</li>
                        <li>Join Cheerlights, the world's biggest IoT project</li>
                        <li>Find the best gesture sensor for your projects</li>
                        <li>Discover the brand new Raspberry Pi 3B+</li>
                        <li>Lots and lots of LEDs </li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/012/original/HS_5_Digital_Optimised.pdf?1521639737" class="button icon fa-download"> Descargar PDF</a>
                    <HR>
                      <h2>HackSpace magazine #6</h2>
                    <p>May 2018 </p>
                   <span class="image fit"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-portada6.png') ?>" alt=""></span>
                      <p align="justify">We learn all about paper engineering and how to cut, fold and stick makes together with just a few sheets. To get you started, HackSpace mag issue 6 comes with a FREE paper automaton to cut-out and make (print edition only).</p>
                    <ul>
                       <li> Make your own gin</li>
                        <li>Build a sumo-wrestling robot</li>
                        <li>The Global Village Construction Set</li>
                        <li>Recreating the world’s first stored program computer</li>
                        <li>Glasses that read aloud</li>
                    </ul>
                    <a href="https://s3-eu-west-1.amazonaws.com/rpi-magazines/issues/full_pdfs/000/000/013/original/HS_6_Digital_Optimised.pdf?1524495009" class="button icon fa-download"> Descargar PDF</a>
                     <HR>

                    <p align="justify">Si te gustó nuestra entrada y quieres adaptarla a tu sitio web, recuerda que éste, al igual que la mayoría de nuestros contenidos, están licenciados bajo Creative Commons (Atribución-Compartir Igual). Encuentra más información sobre esta licencia en: http://creativecommons.org/licenses/by-sa/2.5/co/</p>

                     <div class="3u"><span class="image fit"><img src="<?= base_url('media/images/Yo-Si-Uso-SotwareLibre.png') ?>" alt="" data-position="fixed"></span></div>
                    <p>Fuente: <a href="https://www.cyberhades.com/2017/11/25/hackspace-magazine/" target="_blank">Cyberhades-HackSpace, Tu Revista Gratuita Sobre Hackerspaces</a> por tuxotron licenciado bajo CC BY-SA 2.5 CO <br>Fuente: <a href="https://hackspace.raspberrypi.org/" target="_blank">HackSpace magazines</a> por Raspberry Pi Trading Ltd. licenciado bajo CC BY-NC-SA 3.0</p>




                    </div>
                    </div>
		    </section>
		</div>
	</div>
