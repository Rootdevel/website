<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Blogs</h1>
					</header>

					<hr />
					<dt><h2>MicroPython + Wifi con el Modulo ESP8266 por 4 dolares</h2></dt>
							<span class="image fit"><img src="media/images/micropython/NodeMCU_DEVKIT_1.0.jpg" alt="" /></span>
							<dd>
							<blockquote>"Me ha llegado mi modulo Wifi + plataforma de desarrollo ensamblada por Node MCU que no me a salido por mas de 4 dolares (USD). Desde china hasta mi casa...." by fandres </blockquote>
							<a href="<?= base_url() ?>blogs/micropython" class="button"> Leer Más ...</a>	
							</dd>
			            
			            	<hr />
					<dt><h2>Manifiesto por la prevención de datos</h2></dt>
							<span class="image fit"><img src="media/images/data-prevention/discurso-privacidad.jpeg" alt="" /></span>
							<dd>
							<blockquote>"Las hackers han proclamado que la privacidad está muerta hace décadas, que todo puede y será capturado, almacenado y analizado" </blockquote>
							<a href="<?= base_url() ?>blogs/dataprevention" class="button"> Leer Más ...</a>	
							</dd>	
				

	                <hr />
					<dt><h2>HackSpace magazine, Tu Revista Gratuita Sobre Hackerspaces</h2></dt>
							<span class="image fit"><img src="media/images/hackspace-magazine/hackspace-magazine-2.jpg" alt="" /></span>
							<dd>
							<blockquote>"HackSpace magazine, es la nueva revista mensula para todas aquellas personas que amán hacer las cosas (DIY) y aquellos que quieren aprender; ¡Coger cinta adhesiva, programar un microcontrolador, preparar una impresora 3D y Hackear el mundo que les rodea!"</blockquote>
							<a href="<?= base_url() ?>blogs/hackspacemagazine" class="button"> Leer Más ...</a>	
							</dd>
			            
			         <hr />
                    <dt><h2>Hackerspaces: refugios de creatividad y conocimiento</h2></dt>
							<span class="image fit"><img src="media/images/hackerspaces/hackerspaces_rootdevel.jpg" alt="" /></span>
							<dd>
							<blockquote>"Los hackerspaces surgieron como lugares físicos en los que se reúne gente con intereses comunes, casi siempre afines a la tecnología, la ciencia, la informática y el arte digital, aunque tampoco están cerrados a otros tipos de expresiones."</blockquote>
							<a href="<?= base_url() ?>blogs/hackerspaces" class="button"> Leer Más ...</a>	
							</dd>
			            
			            	<hr />



	           </section>
		</div>
	</div>
