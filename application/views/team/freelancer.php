<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Equipo</h1>
					</header>

          <hr />

          <div class="row">
						<div class="6u 12u$(small)">
							<h3>Sem turpis amet semper</h3>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat commodo eu sed ante lacinia. Sapien a lorem in integer ornare praesent commodo adipiscing arcu in massa commodo lorem accumsan at odio massa ac ac. Semper adipiscing varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						</div>
						<div class="6u$ 12u$(small)">
							<h3>Magna odio tempus commodo</h3>
							<p>In arcu accumsan arcu adipiscing accumsan orci ac. Felis id enim aliquet. Accumsan ac integer lobortis commodo ornare aliquet accumsan erat tempus amet porttitor. Ante commodo blandit adipiscing integer semper orci eget. Faucibus commodo adipiscing mi eu nullam accumsan morbi arcu ornare odio mi adipiscing nascetur lacus ac interdum morbi accumsan vis mi accumsan ac praesent.</p>
						</div>

						<!-- Break -->
						<div class="4u 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/kAoi97.jpg') ?>" alt="" /></a>
							<h3>Leonardo Alvarado</h3>
              <p><h4><a href="">@kAoi97</a></h4></p>
              <ul class="icons">
                <li><a href="https://www.likedin.com/in/kAoi97" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="https://twitter.com/kAoi97" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="https://www.facebook.com/kAoi97" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="https://www.instagram.com/kAoi97" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							</ul>
							<p>El responsable detrás de kAoi97; un Sociólogo, apasionado por la cultura del país del sol naciente, entusiasta en el software y hardware libre, la seguridad informática, la música, la estética, hago algo de trading, y... Ah, lo olvidaba, también cocino.</p>
						</div>
						<div class="4u 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/Exnovus.jpg') ?>" alt="" /></a>
							<h3>Oscar Reyes</h3>
              <p><h4><a href="">@Exnovus</a></h4></p>
              <ul class="icons">
                <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							</ul>
							<p>Activistas por la Cultura Libre y la Colaboración Digital, defensor de la Conciencia Cívica. Co-Founder & CEO de Rootdevel Hackerspace (foundation). #Ciencias #Meditación #Cine #Literatura #Deportes #VideoJuegos.</p>
						</div>
						<div class="4u$ 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/Jaimito.jpg') ?>" alt="" /></a>
							<h3>Accumsan montes viverra</h3>
              <p><h4><a href="">@kAoi97</a></h4></p>
              <ul class="icons">
                <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							</ul>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						</div>

            <!-- Break -->
						<div class="4u 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/Anderson.jpg') ?>" alt="" /></a>
							<h3>Interdum sapien gravida</h3>
              <p><h4><a href="">@kAoi97</a></h4></p>
              <ul class="icons">
                <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							</ul>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						</div>
						<div class="4u 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/Fandres.jpg') ?>" alt="" /></a>
							<h3>Faucibus consequat lorem</h3>
              <p><h4><a href="">@kAoi97</a></h4></p>
              <ul class="icons">
                <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							</ul>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						</div>
						<div class="4u$ 12u$(medium) align-center">
              <a href="#" class="image team"><img src="<?= base_url('media/images/members/Marlon.jpg') ?>" alt="" /></a>
							<h3>Accumsan montes viverra</h3>
              <p><h4><a href="">@kAoi97</a></h4></p>
              <ul class="icons">
                <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
								<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							</ul>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						</div>
					</div>

				</section>

		</div>
	</div>
