<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

							<!-- Nav -->
							<nav id="menu">
								<ul class="links">
								    <li><a href="<?= base_url() ?>">Inicio</a></li>
								    <li><a href="<?= base_url() ?>about">Somos</a></li>
									<li><a href="<?= base_url() ?>blogs">Blogs</a></li>
									<li><a href="<?= base_url() ?>projects">Proyectos</a></li>
									<li><a href="<?= base_url() ?>events">Eventos</a></li>
									<li><a href="<?= base_url() ?>legal/glosary">Glosario</a></li>
									<li><a href="<?= base_url() ?>legal/convivence">Codigo de camaraderia</a></li>
									<li><a href="<?= base_url() ?>legal/law1819">Informes de gestión</a></li>
								</ul>

								<ul class="actions vertical">
								<!--
									<li><a href="#" class="button special fit">Registrate</a></li>
								-->
									<li><a href="#" class="button fit">Iniciar sesion</a></li>
								</ul>

							</nav>
