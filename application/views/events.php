<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Eventos</h1>
					</header>

					<hr />
					<dt><h2>Software Freedom Day</h2></dt>
							<span class="image fit"><img src="media/images/sfd2012.jpg" alt="" /></span>
							<dd>
							<blockquote>"El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor..." </blockquote>
							<a href="<?= base_url() ?>events/sfd2012" class="button"> Leer Más ...</a>	
							</dd>
			            	<hr />
						
							<dt><h2>Festival Latinoamericano de Instalación de Software Libre</h2></dt>
							<span class="image fit"><img src="media/images/flisol.png" alt="" /></span>
							<dd>
								<blockquote>El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aun personas que no poseen mucho conocimiento informático.</blockquote>
							<a href="<?= base_url() ?>events/flisol2014" class="button"> Leer Más ...</a>	
							</dd>
					</section>

		</div>
	</div>
