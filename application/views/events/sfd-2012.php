<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Con éxito se desarrolló el evento “Software Freedom Day”</h1>
					</header>

          <hr />

					<a href="#" class="image main"><img src="<?= base_url('media/images/sfd2012.jpg') ?>" alt="" /></a>

            <p>El “Software Freedom Day” tuvo como principal objetivo dar los beneficios del software libre a conocer a docentes, estudiantes, profesionales, empresarios y a la comunidad en general, acerca de.</p>
            <p>Como ya es habitual siempre estamos colaborando muy de cerca a las comunidades de software libre y hardware abierto de Boyacá y el país. Sobre este tema hay dos eventos principales en los cuales participamos y corresponden al FLISOL (Festival Latinoamericano de Instalación de Software Libre) y al SFD (Software Freedom Day), está vez te traemos un breve resumen de lo que fue el evento organizado por la comunidad del Hackerspace Rootdevel en esta versión 2012.</p>
            <p>El pasado jueves 27 de septiembre de 2012 se llevó a cabo el “Software Freedom Day” en la ciudad de Sogamoso, Boyacá. En el auditorio de la Cámara de Comercio, evento que congregó una vez más a la comunidad software libre y a personas (estudiantes, profesionales y empresarios) interesadas en conocer las alternativas que brinda el software libre al software privativo. Un evento de inclusión tecnológica alrededor de las tecnologías libres como hardware libre, cultura libre y licenciamientos libres.</p>
            <p>Este evento es promovido por un organismo sin ánimo de lucro conocido como 'Digital International Freedom', y es organizado por personas independientes, entidades privadas, pymes, entidades gubernamentales, instituciones educativas y comunidades que trabajan alrededor de estas tecnologías y que buscan dar a conocer las ventajas y beneficios de trabajar con estas tecnologías a toda la comunidad en general.</p>
            <p>El evento contó con la presencia de más de 500 asistentes y 20 ponentes y talleristas; Adicionalmente se tuvo la participación de las comunidades de software libre como: Ubuntu Colombia, Fundación Casa del Bosque, Software Libre Boyacá, cada una de las comunidades apoyaron el evento con la muestra de sus proyectos y iniciativas, así como también con una variada y nutrida sesión de charlas y talleres que se impartieron. además de contar con el patrocinio de microempresas de la región.</p>
            <blockquote>“El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor. La idea fue compartir conocimiento que se relaciona con copyleft o Creative Commons, Es un evento que estuvo dirigido a la comunidad en general, no solo a ingenieros, pues el software libre aplica en muchos contextos”</blockquote>
            <p>El colectivo artístico <strong>Dirty Harry</strong> nos acompaño en cierre del evento haciendo una puesta en escena musical impregnado de moda, fotografía y música electrónica con un sonido fresco y una imagen vanguardista.</p>
            <p>A continuación, el programa de la jornada:</p>

            <div class="video">
            	<iframe src="https://www.youtube.com/embed/EqCAf2wXyKI" frameborder="0" allowfullscreen></iframe>
						</div>

						<div class="video">
            	<iframe src="https://www.youtube.com/embed/6AKPOSOUlrc" frameborder="0" allowfullscreen></iframe>
            </div>

            <br>
            <h2>CONFERENCIAS AUDITORIO JULIAN ASSANGE</h2>

            <ol>
				<li>INGRESO AL AUDITORIO</li>
            <ul>
				<li>Camara de Comercio de Sogamoso.</li>
				<li>Carrera 11 No. 21 – 112</li>
				<li>7:30 AM – 7:50 AM.</li>
			</ul>

              <li>APERTURA SOFTWARE FREEDOM DAY SOGAMOSO 2012</li>
            <ul>
				<li>QUIENES SOMOS Y QUE ES EL SOFTWARE FREEDOM DAY</li>
				<li>Rootdevel Hackerspace</li>
				<li>Ingeniero - Fernando Andres Fernández</li>
				<li>Admon. de redes y datos - Oscar Reyes Hernández</li>
				<li>7:50 AM – 8:10 AM</li>
            </ul>

				<li>INTERVENCION ALCALDE MUNICIPAL DE SOGAMOSO</li>
             <ul>
                <li>SOGAMOSO PUNTO VIVE DIGITAL</li>
                <li>Doctor Miguel Angel Garcia Perez</li>
			</ul>

				<li>INTRODUCCIÓN Y FILOSOFÍA DEL SOFTWARE LIBRE</li>
			<ul>
				<li>Ponente: Fausto Mauricio</li>
                <li>Licenciado en matemáticas y estadística - UPTC</li>
                <li>Docente – Colegio Seminario Diocesano - Duitama</li>
                <li>8:15 AM – 9:00 AM</li>
			</ul>

				<li>SOFTWARE LIBRE EN LAS PYMES EN BOYACÁ</li>
			<ul>
                <li>Ponente: Julian Monsalve</li>
                <li>Ingeniero de Sistemas</li>
                <li>Docente Universidad Santo Tomas - Tunja</li>
                <li>9:05 AM – 9:50 AM</li>
			</ul>

			<li>CLOUD COMPUTING "ADMINISTRACIÓN DE COLEGIO EN LA NUBE"</li>
			<ul>
			    <li>Ponente: Carlos Gerardo Galeano</li>
			    <li>Ingeniero de sistemas</li>
			    <li>Especialista en Desarrollo Web</li>
			    <li>9:55 AM – 10:40 AM</li>
			</ul>

			<li>BREAK</li>
			<ul>
				<li>MUESTRA TECNOLOGICA EMPRESARIAL DE SOGAMOSO</li>
                <li>10:40 AM – 10:55 AM</li>
			</ul>

				<li>ENTORNO DE DESARROLLO PARA DISPOSITIVOS MOVILES CON ANDROID</li>
			<ul>
                <li>Ponente: Jairo Armando Riaño</li>
                 <li>Ingeniero Sistemas – Universidad Antonio Nariño</li>
                 <li>Docente catedratico – UPTC</li>
                 <li>11:00 AM – 12:00 AM</li>
			</ul>

				<li>TIME LUNCH</li>
			<ul>
				<li>12:05 PM – 1:40 PM</li>
			</ul>

				<li>INGRESO AL AUDITORIO</li>
			<ul>
                <li>Cámara de Comercio de Sogamoso</li>
                <li>1:45 PM – 2:00 PM</li>
			</ul>

				<li>DERECHOS DE AUTOR Y CULTURA LIBRE</li>
			<ul>
                 <li>Ponente: Farid Amed</li>
                 <li>Fundacion Casa del Bosque - Bogota</li>
                 <li>2:05 PM – 3:00 PM</li>
			 </ul>

				<li>FRAMEWORKS HTML5 EL FUTURO DE LA WEB</li>
			<ul>
                 <li>Ponente: Tito Agudelo Florez</li>
                 <li>Ponente: Juan Pablo Pinilla</li>
                 <li>Ingeniero de Sistemas – Universidad Piloto - Bogotá</li>
                 <li>Experto en Desarollo Web</li>
                 <li>3:05 PM – 3:50 PM</li>
			</ul>

			<li>HACKING ETICO Y SOFTWARE LIBRE</li>
			<ul>
                 <li>Ponente: José Antonio Cely</li>
				 <li>Programador Senior PHP</li>
                 <li>Experto en Seguridad y Redes</li>
                 <li>4:00 PM – 5:00 PM</li>
			</ul>

			<li>ENTREGA DE PREMIOS Y CIERRE DEL EVENTO</li>
			<ul>
				  <li>Rootdevel Hackerspace</li>
                  <li>5:05 PM – 5:15 PM</li>
			</ul>
			</ol>

			<h2>TALLERES AUDITORIO DENNIS RITCHIE</h2>
			<ol>
				  <li>Acaso, UEFI es una amenaza para GNU/Linux?</li>
			<ul>
                  <li>Ponente: Juan Francisco Mendoza Moreno</li>
                  <li>Ingenierio de Sistemas</li>
                  <li>8:30 AM – 9:30 AM</li>
			</ul>

				  <li>DISPOSITIVOS MOVILES JAVA J2ME</li>
			<ul>
                  <li>Ponente: Fredy León</li>
                  <li>Ingeniero Electrónico - UPTC</li>
                  <li>Empresa: Saset sas</li>
                  <li>Temáticas:</li>
                  <li>Que es Java J2me</li>
                  <li>En que dispositivos trabaja</li>
                  <li>Estructura basica</li>
                  <li>Mi primera aplicacion</li>
                  <li>Preguntas y varios</li>
                  <li>9:31 AM – 10:30 AM</li>
			</ul>

				  <li>INTRODUCCION AL SOFTWARE LIBRE USANDO LINUX MINT DEBIAN EDITION</li>
			<ul>
                  <li>Ponente: Diddier Hilarion</li>
                  <li>Universidad Nacional</li>
                  <li>Bogota-Mesh</li>
                  <li>Temáticas:</li>
                  <li>Introducción al software libre y al open source</li>
                  <li>El FHS, conociendo los directorios de GNU/Linux</li>
                  <li>Conociendo como GNU/Linux maneja los usuarios</li>
                  <li>Conociendo archivos comunes de configuración</li>
                  <li>Conociendo como instalar paquetes por Gestor de software y terminal</li>
                  <li>Tiempo dedicado a solucionar dudas</li>
                  <li>10:31 AM – 12:00 ME</li>
			</ul>

				   <li>TALLER DE ARDUINO HARDWARE LIBRE</li>
			<ul>
			 	  <li>Ponente: Carlos Leonardo Urrego</li>
                  <li>Hackbo - Bogota</li>
                  <li>2:00 PM – 3:00 PM</li>
			</ul>

				<li>SISTEMAS DE SEGURIDAD EN LA RED Y EL SOFTWARE LIBRE</li>
			<ul>
				<li>Ponente: Johan Dario Tellez Bonilla</li>
                <li>Ubuntu - Colombia</li>
                <li>3:05 PM – 4:10 PM</li>
			</ul>
</ol>
<hr />

			<h2>Eventos anteriores, cronología  del SFD Sogamoso</h2>
			<p>
			Año 2012:&nbsp;  <a href="http://wiki.softwarefreedomday.org/2012/Colombia/sogamoso"target="_blank" rel="noopener">SFD 2012 <br></a>
			</p>
		</div>
		</div>
	</section>

</div>
</div>
