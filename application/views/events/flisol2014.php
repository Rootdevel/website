<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>El Festival Latinoamericano de Instalación de Software Libre, FLISoL Sogamoso 2014</h1>
					</header>

          <hr />

					<a href="#" class="image main"><img src="<?= base_url('media/images/flisol.png') ?>" alt="" /></a>

        <p>El pasado  miércoles 14 de mayo, se llevó a cabo el Festival Latinoamericano de Instalación de Software Libre (FLISOL). En el auditorio de la Cámara de Comercio, uno de los eventos tecnológicos más importantes de latinoamerica, festival que se realizó por primera vez en la ciudad  gracias a la comunidad GLUNAD y GISOL; y esta ultima version es organizada por la comunidad del Hackerspace Rootdevel, que continua construyendo una ciudad con oportunidades para acceder al conocimiento, a la tecnología, conservando al máximo los derechos a la libertad y la privacidad.</p>
		<p>Durante el evento se contó con la presencia de más de 800 asistentes y mas de 50 instalaciones de manera gratuita del sistemas operativos GNU/LINUX y las alternativas libres a todas las personas que  llevaron sus equipos a la sede designada.</p>
		<p>El evento sirvio también para brindar información sobre soluciones de bajo costo pero bastante poderosas a pequeñas y medianas empresas,  desde un sistema operativo y un software ofismático, pasando por servidores web hasta llegar a  plantas telefónicas VOIP.</p>
		<blockquote>Sogamoso se lleno de tecnologías libres para marcar un antes y un después en tu vida.</blockquote>
		<p>El Festival Latinoamericano de Instalación de Software Libre (FLISoL) es el evento de difusión de Software Libre más grande en Latinoamerica. Se realiza desde el año 2005 y desde el 2008 se adoptó su realización el 4to Sábado de abril de cada año. En Sogamoso se realizará 14 de Mayo de tal forma que el evento pueda enriquecerse lo suficiente con participantes de otras ciudades del país y a su vez sirva de plataforma para incentivar a los asistentes a asistir al evento en otras ciudades.</p>
		<h2>¿Qué es el FLISoL ? </h2>
		<p>El Festival Latinoamericano de Instalación de Software Libre (FLISoL) es el evento de difusión de Software Libre más grande en Latinoamerica. Se realiza desde el año 2005 y desde el 2008 se adoptó su realización el 4to Sábado de abril de cada año. En Sogamoso se realizará 14 de Mayo de tal forma que el evento pueda enriquecerse lo suficiente con participantes de otras ciudades del país y a su vez sirva de plataforma para incentivar a los asistentes a asistir al evento en otras ciudades.</p>
		<blockquote>Su principal objetivo es promover el uso del software libre, dando a conocer al público en general su filosofía, alcances, avances y desarrollo.</blockquote>
		<p>A tal fin, las diversas comunidades locales de software libre (en cada país/ciudad/localidad), organizan simultáneamente eventos en los que se instala, de manera gratuita y totalmente legal, software libre en las computadoras que llevan los asistentes. Además, en forma paralela, se ofrecen charlas, ponencias y talleres, sobre temáticas locales, nacionales y latinoamericanas en torno al Software Libre, en toda su gama de expresiones: artística, académica, empresarial y social.</p>
		<blockquote>¡Libera tu Equipo! tráelo y te instalamos software libre.</blockquote>
		<blockquote>No importa si tú equipo es "último modelo" o ya tiene muchos años... tenemos diferentes distribuciones GNU/LINUX para que puedas elegir la que más te guste o te convenga.</blockquote>

	<div class="video">
	<iframe src="https://www.youtube.com/embed/lYRMEVCwEfI" frameborder="0" allowfullscreen></iframe>
	</div>
	<div  class="video">
		<iframe src="https://www.youtube.com/embed/U1PF9jRa6fU" frameborder="0" allowfullscreen></iframe>
	</div>

		<h2>Conferencias</h2>
		<p>Las Charlas fueron orientadas a todo público, en las que se dio a conocer temas relacionados con la Cultura Libre (Software Libre, Hardware Libre y Contenidos Libres), entrar en contacto con su filosofía, conocer a otros usuarios, resolver dudas e interrogantes, intercambiar opiniones y experiencias. La entrada fue totalmente libre y gratuita:</p>

		<ol>
					<li>INGRESO AL AUDITORIO</li>
			<ul>
					<li>Camara de Comercio de Sogamoso.</li>
					<li>Carrera 11 No. 21 – 112</li>
					<li>7:30 AM – 7:50 AM.</li>
					<li>Auditorio 1</li>
			</ul>

					<li>APERTURA FLISoL 2014</li>
			<ul>
					<li>Rootdevel Hackerspace</li>
					<li> 8:00 AM</li>
					<li>Auditorio 1</li>
			</ul>

					<li>HACKERSPACES </li>
			<ul>
					<li>Cultura Hacker Verdades, Mitos y Leyendas. Hackerspace un espacio pensado para la inspiración y el ocio productivo; en un mismo lugar convergen las ideas creativas y las herramientas para realizar lo imaginado.</li>
					<li>Ponente: Oscar David Reyes H. </li>
					<li>Hacktivista de software libre y cultura abierta </li>
					<li>Rootdevel Hackerspace</li>
					<li> 8:30 AM</li>
					<li>Auditorio 1</li>
			</ul>

					<li>INTRODUCCION A SOFTWARE LIBRE!!!</li>
			<ul>
					<li>Filosofía, Movimiento y cultura del Software libre: el Software que puede ser usado, copiado, estudiado, modificado y redistribuido libremente.</li>
					<li>Ponente: Oliver Alejandro Moreno C. </li>
					<li>Mantics</li>
					<li>9:20 AM</li>
					<li>Auditorio 1</li>
			</ul>

					<li>MODELADO EN 3D MEDIANTE BLENDER</li>
			<ul>
					<li>Mostrar las principales características de Blender y dar a conocer que podemos llegar a construir con el poder de renderizacion de esta App.</li>
					<li>Ponente: Cesar Abril Medina. </li>
					<li>Grupo GNU/Linux, UPTC </li>
					<li> 10:00 AM</li>
					<li>Auditorio 1</li>
			</ul>
					<li>CONECTANDO RASPBERRY PI CON LA RREALIDAD</li>
			<ul>
					<li>Raspberry Pi es considerada la placa computador mas pequeña y económica del mundo. Este dispositivo es basado en software libre lo cual ofrece la posibilidad de realizar una gran cantidad de aplicaciones.</li>
					<li>Ponente: Edgar Diario Cárdenas Ducon. </li>
					<li>Ingenierío de Sistemas.</li>
					<li> 10:05 AM</li>
					<li>Auditorio 2</li>
			</ul>

			<li>SEGURIDAD EN REDES INFORMÁTICAS</li>
			<ul>
					<li>Aspectos básicos de la seguridad informática en redes de comunicación y organizaciones, políticas y estandares de seguridad informática.</li>
					<li>Ponente: Jorge Hernando Mongui Naranjo. </li>
					<li>Ingeniero de Sistemas, Especialista en Gerencia Informática.</li>
					<li> 10:35 AM</li>
					<li>Auditorio 1</li>
			</ul>

			<li>ENCHULANDO LINUX</li>
				<ul>
					<li>Como podemos modificar a nuestro gusto y dar mejor aspecto a nuestras distribuciones basadas en entornos KDE o GNOM.</li>
					<li>Ponente: Jaime Ernesto Archila Barragán.</li>
					<li>Rootdevel Hackerspace</li>
					<li> 10:40 AM</li>
					<li>Auditorio 2</li>
				</ul>

				<li>LAS REDES SOCIALES LIBRES Y LA COMUNICACIÓN SIN FRONTERAS</li>
					<ul>
						<li>Las redes sociales libres y su importancia mundial. Una radio producida con Software Libre (preproducción, producción y postproducción).</li>
						<li>Ponente: Javier Sneider Bautista.</li>
						</li>Comunicador Social, productor radial, inclusor digital, locutor, fundador y coordinador de @tuiterosboyaca, administrador web, campusero, Community Manager.</li>
						<li>Tuiteros Boyacá</li>
						<li> 11:05 AM</li>
						<li>Auditorio 1</li>
					</ul>

				<li>LA IMPORTANCIA DE LAS LICENCIAS CREATIVE COMMONS COMO HERRAMIENTA PARA GARANTIZAR EL ACCESO A LA INFORMACIÓN Y EL CONOCIMIENTO</li>
					<ul>
						<li>La importante misión de Creative Commons de ayudar a las personas a compartir el conocimiento, y la creatividad con el mundo, a través de sus licencias gratuitas y más flexibles, que amplían el alcance del tradicional derecho de autorio.</li>
						<li>Ponente: Diego Felipe Caballero.</li>
				</li>Estudiante Ultimo Semestre Derecho U. ANDES. </li>
						<li>Creative Commons Colombia</li>
						<li> 11:05 AM</li>
						<li>Auditorio 2</li>
					</ul>

				<li>INTRODUCCIÓN A LAS HERRAMIENTAS DE DESARROLLO LIBRE RUBY & PYTHON</li>
					 <ul>
						<li>En los entornos de desarrollo de software se han venido adoptando herramientas que permitan una fácil interacción con el usuario en entornos web y que a la vez se pueda expresar la lógica del negocio en lenguajes de fácil aprendizaje. Ruby y Python son lenguajes que cumplen las anteriores características reduciendo la curva de aprendizaje y los tiempos de desarrollo al combinarlos con Framework.</li>
						<li>Ponentes:  Juan Fernando Vega Pineda / Edison Alexánder Díaz Álvarez.</li>
						</li>Grupo Ruby / Grupo Python Universidad Pedagogica y Tecnólogica de Colombia</li>
						<li> 11:45 AM</li>
						<li>Auditorio 2</li>
					</ul>

				<li>MUJERES Y SOFTWARE LIBRE</li>
					<ul>
						<li>Participación de la mujeres desde los inicios de desarrollo tecnologico, el por que a historia ha invisibilizado su participacion, historia de las mujeres pioneras en desarrollo tecnologico, y Comunidades de mujeres con SL en el Mundo.</li>
						<li>Ponente: Nancy Johana Ruiz Patiño.</li>
						<li>Tecnologa Gestion redes de datos - Estuduante Ingeniería de Sistemas </li>
						<li>Rootdevel Hackerspace</li>
						<li> 11:45 AM</li>
						<li>Auditorio 1</li>
					</ul>

				<h3> RECESO </h3>

				<li>TELEFONIA VOIP ASTERISK</li>
					<ul>
						<li>Servicios de internet, telefonia IP, Asterisk, freePBX, Bajo GNU/Linux Debian.</li>
						<li>Ponente: Javier chavez.</li>
						<li>Empresario, dedicado a utilizar las herramientas informáticas del software libre. </li>
						<li>Axsatel Comunicaciones</li>
						<li> 1:50 PM</li>
						<li>Auditorio 1</li>
					</ul>

					<li>GESTIÓN DEL CONOCIMIENTO EN LA ENSEÑANZA DE LA INSTALACIÓN Y USO DEL SISTEMA OPERATIVO FEDORA (GNU/LINUX)</li>
					<ul>
						<li>Instalación y uso del sistema operativo FEDORA, tomando como muestra un grupo de estudiante de ingeniería de sistemas y computación comprendido entre los semestres, séptimo(7) , octavo(8) noveno(9), durante el segundo semestre académico del año.</li>
						<li>Ponente: Leissy Esperanza Mancipe V.</li>
						<li>Estudiante Ultimo Semestre Ingeniería de Sistemas UPTC</li>
						<li> 2:00 PM</li>
						<li>Auditorio 2</li>
					</ul>

					<li>VENTAJAS Y APLICACIONES DE LA COMPUTACIÓN DE ALTA DISPONIBILIDAD EN CÓNDOR</li>
					<ul>
						<li>La computación de alta disponibilidad, (High Throughput Computing - HTC), es un modelo de computación distribuida que básicamente busca crear un entorno enfocado a completar el mayor número de trabajos en un amplio periodo de tiempo, lo cual tiene como ventaja el uso eficiente de los recursos disponible.</li>
						<li>Ponentes: Tatiana Blanco R. / Frey Alfonso Santamaría. </li>
						<li>Ingeniería de Sistemas Especialista en Telemática UPTC </li>
						<li>Grupo GNU/Linux, UPTC</li>
						<li> 2:25 PM</li>
						<li>Auditorio 1</li>
						</ul>

					<li>SOFTWARE LIBRE EN LA EDUCACIÓN – PROYECTOS USTA TUNJA</li>
					<ul>
						<li>Importancia del software libre en la educación, algunos casos de éxito y proyectos que se están realizando localmente.</li>
						<li>Ponente: Fredy Andres Aponte Novoa.</li>
						<li>Ingeniero en Sistemas y Computación Magister en Software Libre. </li>
						<li>Software Libre Boycá</li>
						<li> 3:00 PM</li>
						<li>Auditorio 1</li>
					</ul>

					<li>OPEN ACCESS EN LA INVESTIGACIÓN</li>
					<ul>
						<li>Realizar un recorrido en los estados de arte del open Access a nivel mundial y justificar iniciativas de acceso libre en los materiales científicos en Colombia y el Mundo.</li>
						<li>Ponente: Julián Alberto Monsalve Pulido. </li>
						<li>Ingeniero de Sistemas Maestría en Software Libre, Especialista: En desarrollo de aplicaciones Software Libre Boycá. </li>
						<li>Software Libre Boycá</li>
						<li> 3:35 PM</li>
						<li>Auditorio 1</li>
					</ul>

					<li>INCLUSIÓN DE APPS PARA LA ENSEÑANZA DEL CÁLCULO EN EL CONTEXTO UNIVERSITARIO</li>
					<ul>
						<li>El desmos es un programa que funciona en línea por tanto Funcions-Win32 es un excelente programa para trabajar en computador, tabletas y en móviles Smartphone, el cual permite diseñar mediante expresiones algebraicas dibujos artístico desde trazos simples a complejos.</li>
						<li>Ponente: Jose Vicente Samaca Ramirez. </li>
						<li>Docente de Matemáticas, Estudiante de Maestría en Pedagogía</li>
						<li>Universidad Santo Tomás, Seccional Tunja</li>
						<li> 4:10 PM</li>
						<li>Auditorio 1</li>

					</ul>
					<li>FREDYCRUD SOFTWARE CASEO</li>
					<ul>
						<li>Proyecto de Maestría realizado por el autor, que consiste en que genera automáticamente código fuente en PHP y JAVA para crear interfaces CRUD (Consulta, actualización, borrado y eliminación).</li>
						<li>Ponente: Juan Francisco Mendoza Moreno. </li>
						<li>Ingeniero de Sistemas Magister en Software Libr, Especialista: En telemática y gerencia de instituciones de educación superior</li>
						<li>Universidad Santo Tomás, Seccional Tunja</li>
						<li> 4:40 PM</li>
						<li>Auditorio 1</li>
					</ul>

					<li>MEJORES PRÁCTICAS Y TECNOLOGÍAS PARA DESARROLLO DE SOFTWARE PROFESIONAL</li>
					<ul>
						<li>Como montar un entorno profesional para el desarrollo de software, desde los servidores, hasta el versionamiento de código, seguimiento a bugs, etc.Todo lo anterior usando tecnologías Open Source.</li>
						<li>Ponente: Jose Antonio Cely. </li>
						<li>Consultor, unixero, desarrollador. Especialista en complicación de proyectos... ¿Especulador tecnológico o económico?</li>
						<li>Tecsua S.A.S</li>
						<li> 4:53 PM</li>
						<li>Auditorio 2</li>
					</ul>
				<hr />

					<h2>Eventos anteriores, cronología del Flisol Sogamoso</h2>

					<p>

						Año 2014:&nbsp;  <a href="https://flisol.info/FLISOL2014/Colombia/Sogamoso"target="_blank" rel="noopener">FLISOL 2014 <br></a>
						Año 2011:&nbsp;  <a href="https://flisol.info/FLISOL2011/Colombia/Sogamoso"target="_blank" rel="noopener">FLISOL 2011 <br></a>
						Año 2009:&nbsp;  <a href="https://flisol.info/FLISOL2009/Colombia/Sogamoso"target="_blank" rel="noopener">FLISOL 2009 <br></a>
						Año 2008:&nbsp;  <a href="https://flisol.info/FLISOL2008/Colombia/Sogamoso"target="_blank" rel="noopener">FLISOL 2008 <br></a>
						Año 2006:&nbsp;  <a href="https://flisol.info/FLISOL2006/Colombia/Sogamoso"target="_blank" rel="noopener">FLISOL 2006 <br></a>

					</p>
				</div>
				</div>
			</section>

	</div>
</div>
