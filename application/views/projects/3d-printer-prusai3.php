<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Prusa i3, una impresora 3D Open Source cuyas piezas se pueden imprimir</h1>
					</header>

          <hr />
					<center>
						<div class="6u">
							<a class="image main"><img src="<?= base_url() ?>media/images/3d-printing/3d-prusa.gif" alt="" /></a>
						</div>
					</center>

					<p align="justify">La impresión 3D es una tecnología que permite la creación de objetos físico usando como base un modelo digital. A través de un proceso de capas, se va superponiendo material (proceso conocido como fabricación aditiva) para lograr construir un objeto. Por su versatilidad ofrece posibilidades infinitas de aplicación. En esta conferencia vamos a explorar el proceso de creación usando la impresión 3D, incluyendo el análisis de las posibilidades de negocio existentes y el estado del arte en cuanto a arquitectura de las impresoras.</p>

					<div class="box">
						<center><h2>Tipos de manufactura</h2></center>
						<center>
							<div class="table-wrapper">
								<table>
									<tbody>
										<tr>
											<td><strong>A-1.</strong></td>
											<td><strong>Acumulativa:</strong> donde se juntan varias partes y se obtiene una pieza</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\acumulativa.svg') ?>" alt="Piezas engranaje">Etapas del proceso piezas engranaje
									</span>
								</div>

								<h3>Conformativa</h3>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>A-2.</strong></td>
												<td>Inyeccion</td>
											</tr>
											<tr>
												<td><strong>A-3.</strong></td>
												<td>Plegado</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\conformativa.jpg') ?>" alt="Extrusión-soplado">Etapas del proceso de extrusión-soplado
									</span>
								</div>

								<h3>Sustractiva</h3>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>A-4.</strong></td>
												<td>Fresado</td>
											</tr>
											<tr>
												<td><strong>A-5.</strong></td>
												<td>Mecanizado</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\fresado.jpg') ?>" alt="Proceso fresado">Etapas del proceso fresado
									</span>
								</div>

								<h3>Aditiva</h3>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>A-6.</strong></td>
												<td>Impresión 3D</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="4u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\3D-print-process1.gif') ?>" alt="Proceso impresión 3D">Etapas del proceso impresión 3D
									</span>
								</div>

								<h2>Resultados impresos en 3D</h2>
								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\cosas-impresas.jpg') ?>" alt="Resultado impresión 3D">Resultados obtenidos con impresión 3D
									</span>
								</div>

								<h2>Qué tienen en común?</h2>
								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\aditiva.jpg') ?>" alt="RepRap">RepRap Prusa i3
									</span>
								</div>

								<h2>Definición</h2>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>B-1.</strong></td>
												<td>Se refiere al proceso de impresion de un modelo con volumen (3 dimensiones)</td>
											</tr>
											<tr>
												<td><strong>B-2.</strong></td>
												<td>Sucesivas capas de materiales se añaden bajo el control de una computadora</td>
											</tr>
											<tr>
												<td><strong>B-3.</strong></td>
												<td>Estos objetos pueden ser practicamente cualquier forma o geometría</td>
											</tr>
											<tr>
												<td><strong>B-4.</strong></td>
												<td>Se basan en un modelo 3D u otra fuente de datos digitales</td>
											</tr>
										</tbody>
									</table>
								</div>

								<hr>

								<h2>Tecnologías - SLA (Estereolitografía)</h2>
								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\SLA.png') ?>" alt="SLA">
									</span>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\carbon3d.jpg') ?>" alt="SLA"> Carbon3D - SLA
									</span>
								</div>

							<div class="8u">
								<div class="video" >
									<iframe src="https://www.youtube.com/embed/UpH1zhUQY0c" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>

							<hr>
							<h2>Tecnología - SLS (Sinterización Selectiva por Láser)</h2>
							<div class="10u">
								<span class="image fit" style="font-size: medium">
									<img src="<?= base_url('\media\images\3d-printing\impression-3d-sls.jpg') ?>" alt="SLA"> Sinterización Selectiva por Láser
								</span>
							</div>
							<div class="8u">
								<div class="video" >
									<iframe src="https://www.youtube.com/embed/tlwEnqdU4mM" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>

							<hr>
								<h2>Tecnología - FDM (Modelado por deposición fundida)</h2>
								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\FDM-Extrusion.png') ?>" alt="SLA"> Sinterización Selectiva por Láser
									</span>
								</div>

								<div class="8u">
									<div class="video" >
										<iframe src="https://www.youtube.com/embed/Sh78sEHHa5k" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>

								<hr>
								<h2>Del diseño a la impresion - El proceso</h2>
								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\proceso-FDM-1.png') ?>" alt="SLA"> Ejemplo Practico 1 (FDM)
									</span>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\proceso-FDM-2.png') ?>" alt="SLA"> Ejemplo Practico 2 (FDM)
									</span>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\proceso-FDM-3.jpg') ?>" alt="SLA"> Ejemplo Practico 3 (FDM)
									</span>
								</div>


								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>C-1.</strong></td>
												<td>Imagen PNG</td>
											</tr>
											<tr>
												<td><strong>C-2.</strong></td>
												<td>omNomNom</td>
											</tr>
											<tr>
												<td><strong>C-3.</strong></td>
												<td>FreeCAD</td>
											</tr>
											<tr>
												<td><strong>C-4.</strong></td>
												<td>Repetier</td>
											</tr>
											<tr>
												<td><strong>C-5.</strong></td>
												<td>Slic3r</td>
											</tr>
											<tr>
												<td><strong>C-6.</strong></td>
												<td>Impresión</td>
											</tr>
										</tbody>
									</table>
								</div>

								<hr>

								<h2>Proceso - Modelado</h2>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>D-1.</strong></td>
												<td>Diseño asistido por computadora</td>
											</tr>
											<tr>
												<td><strong>D-2.</strong></td>
												<td>software de diseño CAD</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\freeCAD.png') ?>" alt="SLA"> FreeCAD
									</span>
								</div>


								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>D-3.</strong></td>
												<td>Escaneo 3D</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\escaneo-3D.jpg') ?>" alt="SLA"> 3D low cost
									</span>
								</div>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>D-4.</strong></td>
												<td>Verificacion de geometria</td>
											</tr>
											<tr>
												<td><strong>D-5.</strong></td>
												<td>Generación STL</td>
											</tr>
											<tr>
												<td><strong>D-6.</strong></td>
												<td>Tamaño malla</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\geometria-malla.png') ?>" alt="SLA"> 3D low cost
									</span>
								</div>


								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>D-7.</strong></td>
												<td>Validacion STL</td>
											</tr>
											<tr>
												<td><strong>D-8.</strong></td>
												<td>Voladizos</td>
											</tr>
											<tr>
												<td><strong>D-9.</strong></td>
												<td>Resolucion detalle</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\seahorse-3d-model-stl.jpg') ?>" alt="SLA"> FDM ready Split SeaHorse 1
									</span>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\seahorse-3d-model-stl-2.jpg') ?>" alt="SLA"> FDM ready Split SeaHorse 2
									</span>
								</div>

								<hr>
								<h2>Proceso - Laminado</h2>


								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>E-1.</strong></td>
												<td>Configuracion</td>
											</tr>
											<tr>
												<td><strong>E-2.</strong></td>
												<td>Software</td>
											</tr>
											<tr>
												<td><strong>E-3.</strong></td>
												<td>Slic3r</td>
											</tr>
											<tr>
												<td><strong>E-4.</strong></td>
												<td>Skeinfoge</td>
											</tr>
											<tr>
												<td><strong>E-5.</strong></td>
												<td>Cura</td>
											</tr>
											<tr>
												<td><strong>E-6.</strong></td>
												<td>Generar Gcode * Repetier-Host</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\g-code-repetier-1.gif') ?>" alt="Repetier-Host-1"><p>G-Code Verification and Preparation</p>
										<img src="<?= base_url('\media\images\3d-printing\g-code-repetier-2.png') ?>" alt="Repetier-Host-2"><p>Visual screening</p>
										<img src="<?= base_url('\media\images\3d-printing\cura-layer_view.gif') ?>" alt="cura-layer view"><p>La Vista de Capas, en Cura muestra cómo las capas construyen el modelo final</p>
										<img src="<?= base_url('\media\images\3d-printing\cura-move_zoom_rotate.gif') ?>" alt="Repetier-Host-2">Puedes mover, escalar y rotar el modelo para posicionarlo de manera diferente en el área de construcción</p>
									</span>
								</div>

								<hr>
								<h2>Proceso - Impresión</h2>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>F-1.</strong></td>
												<td>Configuracion</td>
											</tr>
											<tr>
												<td><strong>F-2.</strong></td>
												<td>Nivelacion</td>
											</tr>
											<tr>
												<td><strong>F-3.</strong></td>
												<td>Precalentamiento</td>
											</tr>
											<tr>
												<td><strong>F-4.</strong></td>
												<td>Carga STL</td>
											</tr>
											<tr>
												<td><strong>F-5.</strong></td>
												<td>Posicionar</td>
											</tr>
											<tr>
												<td><strong>F-6.</strong></td>
												<td>Generar Gcode *</td>
											</tr>
											<tr>
												<td><strong>F-7.</strong></td>
												<td>Revisar</td>
											</tr>
											<tr>
												<td><strong>F-8.</strong></td>
												<td>Imprimir</td>
											</tr>
										</tbody>
									</table>
									<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\calibracion.jpg') ?>" alt="Calibración"><p>Calibración</p>
										<img src="<?= base_url('\media\images\3d-printing\kapton.jpg') ?>" alt="Cinta Kapton"><p>Cinta Kapton</p>
										<img src="<?= base_url('\media\images\3d-printing\cristal.png') ?>" alt="Cristal o  Espejo"><p>Cristal o  Espejo</p>
									</span>
								</div>

								<hr>
								<h2>Proceso - Postproceso</h2>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>G-1.</strong></td>
												<td>Reparacion</td>
											</tr>
											<tr>
												<td><strong>G-2.</strong></td>
												<td>Ensamble</td>
											</tr>
											<tr>
												<td><strong>G-3.</strong></td>
												<td>Pulido</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\reparacion-piezas.jpg') ?>" alt="Reparación"><p>Reparación</p>
										<img src="<?= base_url('\media\images\3d-printing\ensamblaje.jpg') ?>" alt="Reparación"><p>Ensamblaje</p>
										<img src="<?= base_url('\media\images\3d-printing\post-proceso.jpg') ?>" alt="Post-proceso"><p>Acabados y Pulidos</p>
									</span>
								</div>

								<hr>
								<h2>Parametros pincipales en impresion 3D</h2>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>H-1.</strong></td>
												<td>Altura de capa</td>
											</tr>
											<tr>
												<td><strong>H-2.</strong></td>
												<td>Relleno</td>
											</tr>
											<tr>
												<td><strong>H-3.</strong></td>
												<td>Diámetro de extrusor</td>
											</tr>
											<tr>
												<td><strong>H-4.</strong></td>
												<td>Capas sólidas</td>
											</tr>
											<tr>
												<td><strong>H-5.</strong></td>
												<td>Temperatura</td>
											</tr>
											<tr>
												<td><strong>H-6.</strong></td>
												<td>Volumen de impresión</td>
											</tr>
											<tr>
												<td><strong>H-7.</strong></td>
												<td>Velocidad</td>
											</tr>
											<tr>
												<td><strong>H-8.</strong></td>
												<td>Retracción</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="10u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\altura-impresion.png') ?>" alt="Altura Piezas-1"><p>Altura de Impresión</p>
										<img src="<?= base_url('\media\images\3d-printing\altura-impresion-2.jpg') ?>" alt="Altura Piezas-2"><p>Altura de Impresión</p>
										<img src="<?= base_url('\media\images\3d-printing\relleno.jpg') ?>" alt="Tipos de relleno"><p>Tipos de relleno</p>
										<img src="<?= base_url('\media\images\3d-printing\extrusor.jpg') ?>" alt="Tamaños del Extrusor"><p>Tamaños del Extrusor</p>
										<img src="<?= base_url('\media\images\3d-printing\capas-solidas.png') ?>" alt="Capas"> <p>Capas Solidas</p>
										<img src="<?= base_url('\media\images\3d-printing\temperatura.jpg') ?>" alt="Temperatura"> <p>Temperatura</p>
										<img src="<?= base_url('\media\images\3d-printing\tamaño-30cm.png') ?>" alt="Volumen de impresón"> <p>Volumen de impresón</p>
										<img src="<?= base_url('\media\images\3d-printing\velocidad.jpg') ?>" alt="Volumen de impresón"> <p>Velocidad*</p>
									</span>
								</div>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>*</strong></td>
												<td>Entre 10 y 120 mm/s</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Depende de parte a imprimir</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Primer capa</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Perímetro</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Relleno</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Soporte</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Puentes</td>
											</tr>
											<tr>
												<td><strong>*</strong></td>
												<td>Translacion</td>
											</tr>
										</tbody>
									</table>
								</div>



								<hr>
								<h2>Aspectos a tener en cuenta</h2>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>F-9.</strong></td>
												<td>Software</td>
											</tr>
											<tr>
												<td><strong>F-10.</strong></td>
												<td>Repetier host</td>
											</tr>
											<tr>
												<td><strong>F-11.</strong></td>
												<td>MatterControl</td>
											</tr>
											<tr>
												<td><strong>F-12.</strong></td>
												<td>Cura</td>
											</tr>
											<tr>
												<td><strong>F-13.</strong></td>
												<td>Prepara cama caliente</td>
											</tr>
											<tr>
												<td><strong>F-14.</strong></td>
												<td>Kapton</td>
											</tr>
											<tr>
												<td><strong>F-15.</strong></td>
												<td>Cinsta azul</td>
											</tr>
											<tr>
												<td><strong>F-16.</strong></td>
												<td>Laca</td>
											</tr>
										</tbody>
									</table>
								</div>

								<hr>
								<h2>Materiales para impresion 3D</h2>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>I-1.</strong></td>
												<td>Metales: Oro, Plata, Aluminio</td>
											</tr>
											<tr>
												<td><strong>I-2.</strong></td>
												<td>Fragiles: Vidrio, Cerámica</td>
											</tr>
											<tr>
												<td><strong>I-3.</strong></td>
												<td>Comida: Pasta, Pizza, Chocolate, Chicle</td>
											</tr>
											<tr>
												<td><strong>I-4.</strong></td>
												<td>Otros: Cenizas humanas, Cerveza, Area, Azúcar, Neumáticos, Corcho, Bambú, lino</td>
											</tr>
											<tr>
												<td><strong>I-5.</strong></td>
												<td>Bio, Bio-Tintas (Celulas Vivas-Bio-ink), Huesos, Medicamentos, Piel</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\3D-print-gold.jpg') ?>" alt="Impresion-metales"><p>Imprimiendo Oro </p>
										<img src="<?= base_url('\media\images\3d-printing\vidrio-impresion.jpeg') ?>" alt="Impresion-Vidrio"><p>El MIT consigue la impresión 3D de vidrio</p>
										<img src="<?= base_url('\media\images\3d-printing\impresion-comida.jpg') ?>" alt="Impresion-Comida"><p>"Imprimir" tus pancakes</p>
										<img src="<?= base_url('\media\images\3d-printing\impresion-organo.jpg') ?>" alt="Tejidos y órganos-impresion3d"><p>Producción de Tejidos y órganos por impresión en 3D</p>
									</span>
								</div>

								<hr>

								<h2>Materiales - FDM</h2>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>J-1.</strong>	</td>
												<td>ABS</td>
												<td>PLA</td>
												<td>Flexible</td>
											</tr>
											<tr>
												<td><strong>J-2.</strong>	</td>
												<td>HIPS</td>
												<td>PETG</td>
												<td>Nylon</td>
											</tr>
											<tr>
												<td><strong>J-3.</strong>	</td>
												<td>Fibra de carbono</td>
												<td>ASA</td>
												<td>Policarbonato</td>
											</tr>
											<tr>
												<td><strong>J-4.</strong>	</td>
												<td>Polipropileno</td>
												<td>Metálico</td>
												<td>Madera</td>
											</tr>
											<tr>
												<td><strong>J-5.</strong>	</td>
												<td>PVA</td>
												<td>Carbomorph</td>
												<td>Cemento</td>
											</tr>
										</tbody>
									</table>
								</div>

								<hr>

								<h2>Componentes de impresora 3D</h2>
								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>K-1.</strong></td>
												<td>Ejes: X, Y, Z</td>
											</tr>
											<tr>
												<td><strong>K-2.</strong></td>
												<td>Desplazamiento: Tornillo, Correa dentada</td>
											</tr>
											<tr>
												<td><strong>K-3.</strong></td>
												<td>Extrusor-Head: 1, 2, 3</td>
											</tr>
											<tr>
												<td><strong>K-4.</strong></td>
												<td>Cama-Base: Controlador, Fuente de poder, Motores</td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\componentes-x-y-z.jpg') ?>" alt="Cordenadas de Impresió"><p>Cordenadas de Impresión</p>
										<img src="<?= base_url('\media\images\3d-printing\componentes-extrusor.jpg') ?>" alt="Extrusor"><p>Extrusor</p>
										<img src="<?= base_url('\media\images\3d-printing\cama-base.jpg') ?>" alt="Extrusor"><p>Cama base 30 cm</p>
										<img src="<?= base_url('\media\images\3d-printing\arduino.jpg') ?>" alt="Extrusor"><p>Microcontrolador Arduino</p>
										<img src="<?= base_url('\media\images\3d-printing\fuente-prusa.jpg') ?>" alt="fuente-prusa">fuente de alimentacion</p>
										<img src="<?= base_url('\media\images\3d-printing\motor-paso-paso.jpg') ?>" alt="motor-paso-paso">Motor paso apaso</p>
									</span>
									</div>

								<hr>
								<h2>Costos</h2>

								<div class="table-wrapper">
									<table>
										<tbody>
											<tr>
												<td><strong>L-1.</strong></td>
												<td><strong>Directos</strong></strong></td>
												<td><strong>Material, </strong> <strong>Electricidad:</strong> Costo por KwH * 0.2 * Número de Horas</td>
											</tr>
											<tr>
												<td><strong>L-2.</strong></td>
												<td><strong>Indirectos</strong></td>
												<td><strong>Depreciación:</strong> Costo de impresora / 1000 * Números de Horas </td>
											</tr>
											<tr>
												<td><strong>L-3.</strong></td>
												<td><strong>Tiempo de Ensamble</strong></td>
												<td>Mano de obra</td>
											</tr>
										</tbody>
									</table>
								</div>

								<hr>
									<h2>Moda</h2>
										<div class="8u">
											<span class="image fit" style="font-size: medium">
												<img src="<?= base_url('\media\images\3d-printing\moda-3d-printed.jpg') ?>" alt="Impresion-metales"><p>Semana de la Moda de París nueva colección de zapatos impresos en 3D</p>
											</span>
											<a href="http://www.print3dworld.es/2013/07/iris-van-herpen-presenta-en-la-semana-moda-paris-zapatos-impresos-3d.html" target="_blank" rel="noopener" class="button small">Leer noticia</a>
										</div>
									<br>

									<h2>Medio Ambiente</h2>
									<div class="8u">
										<span class="image fit" style="font-size: medium">
											<img src="https://www.impresoras3d.com/wp-content/uploads/2018/02/3a909fac3c60758cc147b99b68e3b9c7.jpeg" alt="Impresion-metales"><p>Utiliza materiales reciclables</p>
										</span>
										<a href="https://www.impresoras3d.com/la-impresion-3d-puede-ayudarnos-mejorar-medioambiente/" target="_blank" rel="noopener" class="button small">Leer noticia</a>
								</div>
								<br>

								<h2>Hogar</h2>
								<div class="8u">
									<span class="image fit" style="font-size: medium">
										<img src="<?= base_url('\media\images\3d-printing\impresion-cosas-home.jpg') ?>" alt="Impresion-metales"><p>Inventos caseros increíbles diseñados por ti que puedes convertir en realidad.</p>
									</span>
									<a href="https://www.youtube.com/watch?v=dqUO7bMKKIo" target="_blank" rel="noopener" class="button small">Leer noticia</a>
								</div>
								<br>


									<h2>Educación</h2>
									<div class="8u">
										<span class="image fit" style="font-size: medium">
											<img src="<?= base_url('\media\images\3d-printing\3D-Educacion.jpg') ?>" alt="Impresion-metales"><p>Las impresoras 3D convierten la experiencia del aprendizaje en un proceso mucho más lúdico y participativo</p>
										</span>
										<a href="http://www.mundodigital.net/los-beneficios-de-las-impresoras-3d-en-la-ensenanza/" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>


									<h2>Oficina</h2>
									<h2>Estructuras complejas</h2>
									<div class="8u">
										<span class="image fit" style="font-size: medium">
											<img src="<?= base_url('\media\images\3d-printing\estructura-compleja.png') ?>" alt="Impresion-metales"><p>Stratasys presenta obras impresas en 3D en la exposición '3D. Imprimir el mundo', en Madrid</p>
										</span>
										<a href="http://imprimalia3d.com/noticias/2017/06/08/009129/stratasys-presenta-obras-impresas-3d-exposici-n-3d-imprimir-mundo-madrid" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>

									<hr>
									<h2>Tendencias</h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>M-1.</strong></td>
													<td>Nivel industrial: partes como turbinas</td>
												</tr>
												<tr>
													<td><strong>M-2.</strong></td>
													<td>Uso medico extendido: protesis personalizdas y tejido</td>
												</tr>
												<tr>
													<td><strong>M-3.</strong></td>
													<td>Personalizaciona A la carta: audifonos a medida</td>
												</tr>
												<tr>
													<td><strong>M-4.</strong></td>
													<td>Innovacion más rápida: protitipado y factibilidad</td>
												</tr>
												<tr>
													<td><strong>M-5.</strong></td>
													<td>Nuevos modelos de impresoras: más rápidasn más economicas</td>
												</tr>
												<tr>
													<td><strong>M-6.</strong></td>
													<td>Nuevos modelos de negocios</td>
												</tr>
												<tr>
													<td><strong>M-7.</strong></td>
													<td>Problemas con los derechos de uso: copias no autorizadas</td>
												</tr>
												<tr>
													<td><strong>M-8.</strong></td>
													<td>Nuevos productos con nuevas propiedades</td>
												</tr>
												<tr>
													<td><strong>M-9.</strong></td>
													<td>Estandarización dentro del currículo de las escualas</td>
												</tr>
											</tbody>
										</table>
									</div>

									<hr>
									<h2>Futuro</h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>N-1.</strong></td>
													<td>Producto localizado</td>
												</tr>
												<tr>
													<td><strong>N-2.</strong></td>
													<td>Eliminacion de la cadena de suministros</td>
												</tr>
												<tr>
													<td><strong>N-3.</strong></td>
													<td>Nuevos materiales</td>
												</tr>
												<tr>
													<td><strong>N-4.</strong></td>
													<td>Celulas: organos funcionales</td>
												</tr>
												<tr>
													<td><strong>N-5.</strong></td>
													<td>Nano impresión</td>
												</tr>
												<tr>
													<td><strong>N-6.</strong></td>
													<td>Diseño por función</td>
												</tr>
												<tr>
													<td><strong>N-7.</strong></td>
													<td>Diseño automatizado</td>
												</tr>
												<tr>
													<td><strong>N-8.</strong></td>
													<td>Impresión 4D</td>
												</tr>
												<tr>
													<td><strong>N-9.</strong></td>
													<td>Cambios de forma ante cambio en el ambiente</td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="8u">
										<span class="image fit" style="font-size: medium">
											<img src="<?= base_url('\media\images\3d-printing\impresion-4d.jpg') ?>" alt="Impresion-metales"><p>La idea detrás de la impresión en 4D , es que se adaptan al entorno con el que interactúan, poseyendo en algunos casos la capacidad de autorrepararse.</p>
										</span>
										<a href="https://www.fayerwayer.com/2015/05/que-es-la-impresion-4d/" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>

									<hr>
									<h2>Algunas noticias de la industria</h2>

									<h3>Sanitas incorpora férulas impresas en 3D en sus tratamientos</h3>
									<div class="10u">
										<span class="image fit" style="font-size: medium">
											<img src="http://imprimalia3d.com/sites/default/files/news/Exovite%202.jpg" alt="Férulas impresas en 3D">Tratamiento Ortopédico de las fracturas
										</span>
										<a href="http://imprimalia3d.com/noticias/2018/07/23/0010197/sanitas-incorpora-f-rulas-impresas-3d-sus-tratamientos" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>
									<br>

									<h3>Bioimpresión 3D de cartílago humano</h3>
									<div class="10u">
										<span class="image fit" style="font-size: medium">
											<img src="https://i0.wp.com/www.elciudadano.cl/wp-content/uploads/2016/02/oreja-3D.jpg?w=630&ssl=1" alt="Bioimpresión 3D">Bioimpresora 3D magnética
										</span>
										<a href="http://imprimalia3d.com/noticias/2018/07/25/0010203/bioimpresi-n-3d-cart-lago-humano" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>
									<br>

									<h3>Reconstruido en 3D el cráneo del dinosaurio carnívoro de Cuenca</h3>
									<div class="10u">
										<span class="image fit" style="font-size: medium">
											<img src="http://imprimalia3d.com/sites/default/files/imagenes/Dino.jpg" alt="carcarodontosáuridos">Cráneo de Concavenato
										</span>
										<a href="http://imprimalia3d.com/noticias/2018/07/24/0010201/reconstruido-3d-cr-neo-del-dinosaurio-carn-voro-cuenca" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>
									<br>

									<h3>Satélite con torre de antena más ligera gracias a la impresión 3D</h3>
									<div class="10u">
										<span class="image fit" style="font-size: medium">
											<img src="http://imprimalia3d.com/sites/default/files/imagenes/Hispasat%201.jpg" alt="Satélite Hipasat 30W-6">Satélite Hipasat 30W-6
										</span>
										<a href="http://imprimalia3d.com/noticias/2018/07/01/0010132/sat-lite-torre-antena-m-s-ligera-gracias-impresi-n-3d" target="_blank" rel="noopener" class="button small">Leer noticia</a>
									</div>
									<br>

									<hr>
									<h2>Ventajas de impresion 3D</h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>Ñ-1.</strong></td>
													<td>Complejidad no influye (gratis)</td>
												</tr>
												<tr>
													<td><strong>Ñ-2.</strong></td>
													<td>Variedad no influye (gratis)</td>
												</tr>
												<tr>
													<td><strong>Ñ-3.</strong></td>
													<td>Esamblaje no influye (gratis)</td>
												</tr>
												<tr>
													<td><strong>Ñ-4.</strong></td>
													<td>Proceso bajo demanda (Cuando se necesita)</td>
												</tr>
												<tr>
													<td><strong>Ñ-5.</strong></td>
													<td>Diseño ilimitado (innovacion)</td>
												</tr>
												<tr>
													<td><strong>Ñ-6.</strong></td>
													<td>Proceso no influye (gratis)</td>
												</tr>
												<tr>
													<td><strong>Ñ-7.</strong></td>
													<td>Fabrica compacta (portable)</td>
												</tr>
												<tr>
													<td><strong>Ñ-8.</strong></td>
													<td>Eficiente (menos desperdicio)</td>
												</tr>
												<tr>
													<td><strong>Ñ-9.</strong></td>
													<td>Replicacion precisa (repetibilidad)</td>
												</tr>
											</tbody>
										</table>
									</div>

									<hr>
									<h2>Ventajas Principales</h2>

									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>O-1.</strong></td>
													<td>Fabricacion en masa</td>
												</tr>
												<tr>
													<td><strong>O-2.</strong></td>
													<td>Fabricacion personalizda</td>
												</tr>
											</tbody>
										</table>
									</div>

									<hr>
									<h2>Limitacion de Impresion 3D</h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>P-1.</strong></td>
													<td>Materiales disponibles</td>
												</tr>
												<tr>
													<td><strong>P-2.</strong></td>
													<td>Velicidad de impresion</td>
												</tr>
												<tr>
													<td><strong>P-3.</strong></td>
													<td>Colores</td>
												</tr>
												<tr>
													<td><strong>P-4.</strong></td>
													<td>Tamaño de impresión</td>
												</tr>
												<tr>
													<td><strong>P-5.</strong></td>
													<td>Consumo de energia</td>
												</tr>
												<tr>
													<td><strong>P-6.</strong></td>
													<td>Copias no autorizadas</td>
												</tr>
												<tr>
													<td><strong>P-7.</strong></td>
													<td>Usos mal intnecionados</td>
												</tr>
												<tr>
													<td><strong>P-8.</strong></td>
													<td>No hay economía de escala</td>
												</tr>
												<tr>
													<td><strong>P-9.</strong></td>
													<td>Gases / Sustancias nocivas</td>
												</tr>
											</tbody>
										</table>
									</div>

									<hr>
									<h2>Algunos Consejos</h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>Q-1.</strong></td>
													<td>Tamaño de capa <0.9 * diámetros de extrusor</td>
												</tr>
												<tr>
													<td><strong>Q-2.</strong></td>
													<td>Usar díametros 0.1 solo cuando sean necesario</td>
												</tr>
												<tr>
													<td><strong>Q-3.</strong></td>
													<td>Usar infill 40% - 20%</td>
												</tr>
												<tr>
													<td><strong>Q-4.</strong></td>
													<td>TTener cuidado con torsion del filamento</td>
												</tr>
												<tr>
													<td><strong>Q-5.</strong></td>
													<td>Calentar extrusores en home antes de empezar a imprimir</td>
												</tr>
												<tr>
													<td><strong>Q-6.</strong></td>
													<td>Computador dedicado 100% o SD (si lo soporta)</td>
												</tr>
												<tr>
													<td><strong>Q-7.</strong></td>
													<td>Quitar SLEEP a computador</td>
												</tr>
												<tr>
													<td><strong>Q-8.</strong></td>
													<td>Buena ventilación</td>
												</tr>
												<tr>
													<td><strong>Q-9.</strong></td>
													<td>Resistencia axonometrica de la pieza</td>
												</tr>
											</tbody>
										</table>
									</div>

									<hr>
									<h2>Preguntas frecuentes sobre las impresoras 3D </h2>
									<div class="table-wrapper">
										<table>
											<tbody>
												<tr>
													<td><strong>-1.</strong></td>
													<td>Compra por diversión o algo serio?</td>
												</tr>
												<tr>
													<td><strong>-2.</strong></td>
													<td>Si es por diversión, entonces version Desktop</td>
												</tr>
												<tr>
													<td><strong>-3.</strong></td>
													<td>Puedo hacer moelos 3d ?</td>
												</tr>
												<tr>
													<td><strong>-4.</strong></td>
													<td>Considerar costos o capacitación</td>
												</tr>
												<tr>
													<td><strong>-5.</strong></td>
													<td>Requerimientos de partes que quiero hacer?</td>
												</tr>
												<tr>
													<td><strong>-6.</strong></td>
													<td>Limitantes de los materiales</td>
												</tr>
												<tr>
													<td><strong>-7.</strong></td>
													<td>Espacio de trabajo?</td>
												</tr>
												<tr>
													<td><strong>-8.</strong></td>
													<td>Espacio para realizar acabados de las piezas</td>
												</tr>
												<tr>
													<td><strong>-9.</strong></td>
													<td>Costos de produccion?</td>
												</tr>
												<tr>
													<td><strong>-10.</strong></td>
													<td>Diseño, Validacion modelos, acabado, pulido</td>
												</tr>
												<tr>
													<td><strong>-11.</strong></td>
													<td>Costos de negocios?</td>
												</tr>
												<tr>
													<td><strong>-12.</strong></td>
													<td>Costos de piezas por mes</td>
												</tr>
												<tr>
													<td><strong>-13.</strong></td>
													<td>Tipos de partes a producir</td>
												</tr>
												<tr>
													<td><strong>-14.</strong></td>
													<td>Materiales similares y usos para justificar</td>
												</tr>
												<tr>
													<td><strong>-15.</strong></td>
													<td>Software?</td>
												</tr>
												<tr>
													<td><strong>-16.</strong></td>
													<td>Costos de software 3D, reparación de archivos</td>
												</tr>
												<tr>
													<td><strong>-17.</strong></td>
													<td>Consumibles?</td>
												</tr>
												<tr>
													<td><strong>-18.</strong></td>
													<td>Costos de materia prima</td>
												</tr>
												<tr>
													<td><strong>-19.</strong></td>
													<td>Uso principal de la impresora 3d?</td>
												</tr>
												<tr>
													<td><strong>-20.</strong></td>
													<td>Involucrar la mayor cantidad de personas</td>
												</tr>
											</tbody>
										</table>
									</div>

									</div>
								</center>

									<h3>Referencias:</h3>
									<ol>
										<li>Ing. Juan Carlos Marino <a href="https://www.youtube.com/watch?v=fk6z5Kl3J44" target="_blank" rel="noopener">Impresión 3D</a></li>
										<li>formizable.com <a href="https://formizable.com/mega-tutorial-de-cura-profundizando-en-cura-3d-slicer/" target="_blank" rel="noopener">Mega Tutorial de Cura: Profundizando en Cura 3D Slicer  </a></li>
									</ol>

									<div class="3u"><span class="image fit"><img src="<?= base_url('media/images/Yo-Si-Uso-SotwareLibre.png') ?>" alt="" data-position="fixed"></span></div>
                  <p>Lo referente a mi aporte se licencia bajo Creative Common by Exnovus. </br>Fuente: <a href=""target="_blank" rel="noopener"> licenciado bajo<a href="https://creativecommons.org/licenses/by/4.0/"target="_blank" rel="noopener">CC BY 4.0</a></p>
								</div>
					 </div>
				 </section>

		 </div>
	 </div>
