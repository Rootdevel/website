<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Conoce el proyecto “Piensa en Grande” Fundación Telefónica</h1>
					</header>

          <hr />

					<a href="#" class="image main"><img src="<?= base_url() ?>media/images/thinkbig2017.jpg" alt="" /></a>

					<blockquote>"Desarrolla competencias y habilidades que promueven el espíritu emprendedor en jóvenes de 14 a 25 años para que se conviertan en ciudadanos responsables, capaces de diseñar su plan de vida y responder a los desafíos del mundo digital".</blockquote>

					<p>El Proyecto Piensa en Grande es una iniciativa de la Fundación Telefónica dirigida al desarrollo de habilidades para la vida y competencias del siglo XXI de jóvenes, pertenecientes a entornos educativos y de programas sociales del país. El énfasis del proyecto está en la transferencia metodológica de Piensa en Grande a los docentes, profesionales, gestores y formadores con la posibilidad de incorporar otras metodologíasbde intervención que enriquezcan su práctica pedagógica y profesional. </p>

					<p>Piensa en Grande se fundamenta en la orientación hacia los jóvenes en edades entre los 14 y 25 años, para descubrir sus potencialidades, talentos y despertar la curiosidad para identificar problemas de su entorno social y actuar ante estos; de manera que se conviertan en ciudadanos responsables, capaces de diseñar su plan de vida y de resolver los desafíos del mundo digital. </p>

					<blockquote>Promover el emprendimiento social es uno de los objetivos de "Piensa en Grande", un proyecto liderado por la Fundación Telefónica y la Dirección TIC de la Gobernación de Boyacá, en cabeza de William Orlando Vargas​​​​​​​, gracias a esta alianza se brindado acompañamiento, asesoría y capacitación a 2.562 padres de familia con Aulas TIC Familia, 2.694 educadores con Aulas Fundación Telefónica y 5.034 estudiantes con "Piensa en Grande", durante el periodo 2016-2017.</blockquote>

					<div class="video">
						<iframe src="https://www.youtube.com/embed/HTUMyP7IMqI" frameborder="0" allowfullscreen></iframe>
					</div>

					<p>Piensa en Grande se desarrolla durante un periodo de seis meses y cuenta con una metodología de la que hacen parte cuatro momentos: el primero de ellos es “Descubrirme”, en el que se orienta a los jóvenes en el descubrimiento de sus talentos, habilidades y oportunidades mediante un aprendizaje significativo y creativo. El segundo es “Integrarme”, donde los estudiantes definen los equipos con los cuales trabajarán y  afianzan competencias como liderazgo, trabajo colaborativo y comunicación.</p>

					<p>“Despertarme” es la tercera etapa del proyecto y en ella se busca acompañar a los jóvenes en el diseño de una iniciativa social que pueda impactar a su comunidad, a través de la metodología Design Thinking (pensamiento de diseño o diseño centrado en el usuario). Allí se promueven competencias como innovación, creatividad, emprendimiento social y gestión de proyectos.</p>

					<p>El cuarto y último momento es “Proyectarme”, en donde se apoya a los jóvenes en la validación e implementación de la iniciativa social formulada. Durante esta etapa se hace énfasis en áreas como gestión de proyectos, habilidades digitales y resolución de conflictos.</p>
				</div>
				</div>
			</section>

	</div>
</div>
