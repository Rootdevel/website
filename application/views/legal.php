<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Legal</h1>
						<p></p>
					</header>

					<hr />

					<dl>
						<a href="">
							<dt>Protitopo digital de una CNC (Control Numérico Computarizado).</dt>
							<dd>
								<p>Estos sistemas de control computerizado han permitido disminuir los tiempos de producción, y sobretodo hacer accesible el uso de estas herramientas sin la necesidad de acumular años de experiencia en su manejo.</p>
							</dd>
						</a>

						<a href="<?= base_url() ?>events/sfd2012">
							<dt>Software Freedom Day</dt>
							<dd>
								<p>"El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor..."</p>
							</dd>
						</a>

						<a href="<?= base_url() ?>events/flisol2014">
							<dt>Festival Latinoamericano de Instalación de Software Libre</dt>
							<dd>
								<p>El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aun personas que no poseen mucho conocimiento informático.</p>
							</dd>

						<a href="<?= base_url() ?>events/thinkbig2017">
							<dt>Conoce el proyecto “Piensa en Grande” Fundación Telefónica</dt>
							<dd>
								<p>"Piensa en Grande se fundamenta en la orientación hacia los jóvenes en edades entre los 14 y 25 años, para descubrir sus potencialidades, talentos y despertar la curiosidad para identificar problemas de su entorno social y actuar ante estos..."</p>
							</dd>
						</a>
					</dl>
				</section>

		</div>
	</div>
