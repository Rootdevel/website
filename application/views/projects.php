<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Proyectos</h1>
					</header>

					<hr />
					<center>
					<dt><h2>Prusa i3, una impresora 3D Open Source cuyas piezas se pueden imprimir</h2></dt>
						<div class="6u">
							<span class="image fit"><img src="media/images/3d-printing/3d-prusa.gif" alt="" /></span>
						</div>

							<dd>
							<blockquote>"Una de las ventajas de poseer una impresora 3D de código abierto como la Prusa i3 es que todas sus piezas de plástico son perfectamente imprimibles desde la misma máquina u otra impresora 3D, por lo que podemos llegar a ‘clonar‘ el propio equipo desde casa sin tener ningún problema legal ..." </blockquote>
							<a href="<?= base_url('projects/prusa3d') ?> " class="button"> Leer Más ...</a>
							</dd>
							<hr />
					<hr />
					<dt><h2>Protitopo digital de una CNC (Control Numérico Computarizado)</h2></dt>

							<span class="image fit"><img src="media/images/CNC-Animation.gif" alt="" /></span>

							<dd>
							<blockquote>"El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor..." </blockquote>
							<a href="<?= base_url() ?> " class="button"> Leer Más ...</a>
							</dd>


							<hr />
							<dt><h2>Conoce el proyecto “Piensa en Grande” Fundación Telefónica</h2></dt>
							<span class="image fit"><img src="media/images/thinkbig2017-Animation.gif" alt="" /></span>

							<dd>
								<blockquote>"Piensa en Grande se fundamenta en la orientación hacia los jóvenes en edades entre los 14 y 25 años, para descubrir sus potencialidades, talentos y despertar la curiosidad para identificar problemas de su entorno social y actuar ante estos..."</blockquote>
							<a href="<?= base_url('projects/thinkbig2017') ?>" class="button"> Leer Más ...</a>
							</dd>
							</center>
					</section>

		</div>
	</div>
