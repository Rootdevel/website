<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Section -->
								<section class="main">
									<a name="about"></a>
									<div class="features">
										<section>
											<a href="<?= base_url('about/#whousare') ?>">
											<span class="icon fa-users major accent3"></span>
											<h3>Quienes Somos</h3>
											<p>...Un grupo interdisciplinar de personas que dedican sus esfuerzos a compartir conocimiento libre en las diferentes áreas del saber... <span class="">ver mas</span></p>
											</a>
										</section>
										<section>
											<a href="<?= base_url('about/#Whatusdo') ?>">
											<span class="icon fa-cogs major accent2"></span>
											<h3>Que hacemos</h3>
											<p>Como organización sin ánimo de lucro [...] concentramos recursos y conocimiento para promover, integrar y desarrollar proyectos...<span class="">ver mas</span></p>
											</a>
										</section>
										<section>
											<a href="<?= base_url('about/#foryou') ?>">
											<span class="icon fa-handshake-o major accent1"></span>
											<h3>Lo que hacemos por ti</h3>
											<p>Hacer que nuestro Hackerspace sea un lugar propicio para que puedas compartir, generar conocimiento...<span class="">ver mas</span></p>
											</a>
										</section>
									</div>
								</section>
