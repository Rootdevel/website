<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

            <section class="main">
                <header class="major">
                  <a name="about"></a>
                  <h2>Principio Orgánico</h2>
                  <p>&nbsp;</p>
                </header>
                <div class="spotlights">
                  <article>
                    <div class="video-landing">
                      <iframe src="https://www.youtube.com/embed/WkiX7R1-kaY" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="content">
                      <h3>¿La proxima revolución?</h3>
                      <p>Mitch Altman haba sobre sus creaciones y la comunidad Hacker, su concepción acerca de cómo no solamente se pueden hackear elementos tecnológicos, sino además el reparto de lo sensible, lo científico, lo político y en general todo aquellas inquietudes del ser humano.</p>
                      <!--<ul class="icons">
                        <li><a target="_blank" href="http://www.facebook.com/rootdevel" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a target="_blank" href="http://twitter.com/rootdevel" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a target="_blank" href="http://www.telegram.com/rootdevel" class="icon fa-telegram"><span class="label">Telegram</span></a></li>
                        <li><a target="_blank" href="https://www.flickr.com/photos/rootdevel_hackerspace/" class="icon fa-flickr"><span class="label">Instagram</span></a></li>
                        <li><a target="_blank" href="http://github.com/rootdevel" class="icon fa-github"><span class="label">GitHub</span></a></li>
                        <li><a target="_blank" href="http://www.soundcloud.com/rootdevel" class="icon fa-soundcloud"><span class="label">Soundcloud</span></a></li>
                      </ul>-->
                    </div>
                  </article>
                  <article>
                    <div class="image"><img src="<?= base_url('media/images/anonymous.jpg') ?>" alt="" data-position="center" /></div>
                    <div class="content">
                      <h3>¿Que se dice de ser un hacker?</h3>
                      <p>"Quien disfruta explorando sistemas y cómo forzar sus capacidades. También se considera como tal a un experto en un programa particular o que escribe código de forma entusiasta, a veces obsesiva. Además, puede considerarse hacker a una persona apasionada por temas no relacionados con la informática, como un hacker de la astronomía y, en general, quien disfruta del reto intelectual de la creación y la superación de las limitaciones".<br /><i>Eric S. Raymond</i></p>
                      <ul class="actions">
                        <li><a href="http://www.dicosmo.org/Piege/ES/www.glug4.linux.org.ar/documentos/comoserhacker.pdf" class="button">Principios Hacker</a></li>
                      </ul>
                    </div>
                  </article>
                </div>
              </section>
