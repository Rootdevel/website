<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

							<!-- Banner -->
								<section id="banner">
									<div class="items">
										<section class="accent3">
											<h1><?= $this->lang->line('landing_banner_first_title');?></h1>
											<p><?= $this->lang->line('landing_banner_first_pharagraph');?></p>
											<ul class="actions">
												<li><a href="#about" class="button special"><?= $this->lang->line('landing_banner_first_button');?></a></li>
											</ul>
										</section>
										<section class="accent2">
											<i class="fa fa-telegram fa-3x"></i><br /><br />
											<h1><?= $this->lang->line('landing_banner_second_title');?></h1>
											<p></p>
											<ul class="actions">
												<li><a href="http://t.me/rootdevel" target="_blank" class="button special icon fa-telegram"><?= $this->lang->line('landing_banner_second_button');?></a></li>
											</ul>
										</section>
									</div>
									<div class="slider">
										<?= $this->sliders->buildLandingSlider();;?>
									</div>
								</section>
