<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Section -->
									<section class="carousel accent4">
										<header class="major">
											<a name="about"></a>
											<h2>Mientras tanto en el hackerspace...</h2>
											<p>A continuacion encontraras un listado de nuestros proyectos, actividades, y eventos llevados a cabo por la comundad de Rootdevel Hackerspace</p>
										</header>
										<article>
											<div class="image"><img src="<?= base_url('media/images/CNC-Animation.gif') ?>" alt="" data-position="center" /></div>
											<div class="content">
												<h3>Protitopo digital de una CNC (Control Numérico Computarizado)</h3>
												<p>Estos sistemas de control computerizado han permitido disminuir los tiempos de producción, y sobretodo hacer accesible el uso de estas herramientas sin la necesidad de acumular años de experiencia en su manejo.</p>
											</div>
										</article>
										<article>
											<a href="<?= base_url('events/sfd2012') ?>">
											<div class="image"><img src="<?= base_url('media/images/sfd2012.jpg') ?>" alt="" data-position="center" /></div>
											<div class="content">
												<h3>Software Freedom Day</h3>
												<p>"El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor..."</p>
											</div>
										</a>
										</article>
										<article>
											<a href="<?= base_url('events/flisol2014') ?>">
											<div class="image"><img src="<?= base_url('media/images/flisol.png') ?>" alt="" data-position="center" /></div>
											<div class="content">
												<h3><a href="<?= base_url('events/flisol2014') ?>">Festival Latinoamericano de Instalación de Software Libre</a></h3>
												<p>El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aun personas que no poseen mucho conocimiento informático.</p>
											</div>
										</a>
										</article>
										<article>
										    <a href="<?= base_url('events/thinkbig2017') ?>">
											<div class="image"><img src="<?= base_url('media/images/thinkbig2017-Animation.gif') ?>" alt="" data-position="center" /></div>
											<div class="content">
												<h3><a href="<?= base_url('events/thinkbig2017') ?>">Conoce el proyecto “Piensa en Grande” Fundación Telefónica</a></h3>
												<p>"Piensa en Grande se fundamenta en la orientación hacia los jóvenes en edades entre los 14 y 25 años, para descubrir sus potencialidades, talentos y despertar la curiosidad para identificar problemas de su entorno social y actuar ante estos..."</p>
											</div>
										</a>
										</article>
										<article>
										    <a href="<?= base_url('blogs/micropython') ?>">
											<div class="image"><img src="<?= base_url('media/images/micropython/NodeMCU_DEVKIT_1.0.jpg') ?>" alt="" data-position="center" /></div>
											<div class="content">
												<h3><a href="<?= base_url('blogs/micropython') ?>">MicroPython + Wifi con el Modulo ESP8266 por 4 dolares</a></h3>
												<p>"Me ha llegado mi modulo Wifi + plataforma de desarrollo ensamblada por Node MCU que no me a salido por mas de 4 dolares (USD). Desde china hasta mi casa...." By fandres</p>
											</div>
										</a>
										</article>
										<article>
                                    			    <a href="<?= base_url('blogs/hackspacemagazine') ?>">
                                    					<div class="image"><img src="<?= base_url('media/images/hackspace-magazine/hackspace-magazine-3.jpg') ?>" alt="" data-position="center" />
                                    					</div>
                                    					<div class="content">
                                    						<h3><a href="<?= base_url('blogs/hackspacemagazine') ?>">HackSpace magazine, Tu Revista Gratuita Sobre Hackerspaces</a></h3>
                                    						<p>"HackSpace magazine, es la nueva revista mensula para todas aquellas personas que amán hacer las cosas (DIY) y aquellos que quieren aprender; ¡Coger cinta adhesiva, programar un microcontrolador, preparar una impresora 3D y Hackear el mundo que les rodea!"</p>
                                    					</div>
                                    				</a>
                                    	</article>


										<nav>
											<a href="#" class="previous"><span class="label">Previous</span></a>
											<a href="#" class="next"><span class="label">Next</span></a>
										</nav>
									</section>
