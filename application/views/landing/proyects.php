<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Wrapper -->
	<div class="wrapper">
		<div class="inner">

			<!-- Main -->
				<section class="main">

					<header class="major">
						<h1>Proyectos</h1>
					</header>

					<hr />
					<dt><h2>Protitopo digital de una CNC (Control Numérico Computarizado)</h2></dt>
							<span class="image fit"><img src="media/images/CNC-Animation.gif" alt="" /></span>
							<dd>
							<blockquote>"El software freedom day es un evento educativo que está orientado a trabajar con tecnologías libres y software libre, lo cual no implica licencias cerradas o derechos de autor..." </blockquote>
							<a href="<?= base_url() ?> " class="button"> Leer Más ...</a>
							</dd>
			            	<hr />

							<dt><h2>Conoce el proyecto “Piensa en Grande” Fundación Telefónica</h2></dt>
							<span class="image fit"><img src="media/images/thinkbig2017-Animation.gif" alt="" /></span>
							<dd>
								<blockquote>"Piensa en Grande se fundamenta en la orientación hacia los jóvenes en edades entre los 14 y 25 años, para descubrir sus potencialidades, talentos y despertar la curiosidad para identificar problemas de su entorno social y actuar ante estos..."</blockquote>
							<a href="<?= base_url('events/thinkbig2017') ?>" class="button"> Leer Más ...</a>	
							</dd>
					</section>

		</div>
	</div>
